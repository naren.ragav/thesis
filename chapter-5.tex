\chapter{Evaluation of Columnar to Equiaxed Transition (CET)}
\label{ch:chapter_5}
\section{Motivation}
This chapter investigates the feasibility of columnar to equiaxed transition during solidification of Inconel 718 in electron beam AM process. Components fabricated in metal AM process tend to have highly textured columnar grains along the build direction \cite{liu2011effect, antonysamy2012microstructure, jia2014selective, murr2012metal}. Feasibility of controlling the solidification grain morphology adds another dimensional freedom to the AM process. As discussed in \Cref{ch:chapter_3}, the grain morphology of metals during rapid solidification processes is dictated by the thermal gradient (G) and the velocity or growth rate (R) at the liquid-solid interface of the melt pool. The G and R can be controlled by controlling the process parameters during fabrication. In general, for any powder bed metal AM process, multiple input parameters affect the solidification behavior. The availability of numerous controllable input parameters (power, velocity, scan strategy, layer thickness, etc) in EBM process means that the control strategy becomes enormous. Hence, experimental verification and validation of multiple input parameters becomes an expensive process in terms of cost and time. To reduce the experimental expense, in this chapter, the numerical modeling and statistical analysis is done apriori to the experimental investigations. Results in this chapter are published in the journal \cite{raghavan2016numerical}.

\section{Qualitative Analysis of Parameters}
Based on the prior published literature by Dehoff et al.\cite{dehoff2015site}, spot melt scan strategy was chosen in place of raster melt scan strategy to investigate the bulk columnar to equiaxed transition. Four process parameters of the spot melt scan strategy were selected for the comprehensive numerical experimentation. The chosen controllable input process parameters were
\begin{description}
  \item[$\bullet$] Electron beam diameter
  \item[$\bullet$] Electron beam current
  \item[$\bullet$] Spot ON time
  \item[$\bullet$] Preheat temperature
\end{description}   
The chosen process parameters are applicable to each of the spots in the melting strategy. Since electron beam accelerating voltage is constant (60 kV), the electron beam current was used as the variable to change the power deposited at a spot. Spot ON time is the dwell time of the electron beam at a particular spot. The preheat temperature is the temperature of the substrate or region nearby to that particular spot. This factor cannot be explicitly controlled like electron beam ON time and diameter but can be controlled by coupling it with the scan strategy. This is explained in the later part of this chapter. 
\\[8pt]
As explained in \Cref{subsec:GR_calc_ch3}, Truchas was used to simulate a single spot with beam ON time of \SI{1}{\milli\second}, preheat temperature of \SI{1528}{\kelvin} (solidus) and beam current of \SI{20}{\milli\ampere}. The G and R data were extracted at each of the nodal elements in the spatial domain as explained in \Cref{subsec:GR_calc_ch3}. The extracted data in \Cref{fig:3_GRVariation} were the plotted on a reference solidification map of IN718. \Cref{fig:5_GRref} shows the G vs R plot on a reference solidification map \cite{nastac2001advances} of IN718. From \Cref{fig:5_GRref}, it is evident that grain morphology is columnar as the melt pool begins to solidify and it moves towards mixed region during solidification and results in an equiaxed region at the end of solidification.

\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_GRref.jpg}
\caption{G vs. R plotted on reference solidification map \cite{nastac2001advances} for the simulation with beam current of \SI{20}{\milli\ampere} and ON time of \SI{1}{\milli\second}. }
\label{fig:5_GRref}
\end{figure}

To do a qualitative analysis of the effect of the process variables on the distribution of G and R, simulations have been performed for different values of the input variables which were changed one by one while the value of other input variables were kept constant. For example, 4 different preheat conditions have been simulated while keeping beam current, diameter and ON time constant for all 4 simulations. In a similar manner, 12 more simulations have been performed for rest of the variables. Values of the variables used in the simulations are
\begin{description}
  \item[$\bullet$] Electron beam diameter - $\SI{200}{\micro\meter}$, $\SI{400}{\micro\meter}$, $\SI{600}{\micro\meter}$, $\SI{800}{\micro\meter}$
  \item[$\bullet$] Electron beam current - $\SI{5}{\milli\ampere}$, $\SI{10}{\milli\ampere}$, $\SI{15}{\milli\ampere}$, $\SI{20}{\milli\ampere}$
  \item[$\bullet$] Spot ON time - $\SI{0.1}{\milli\second}$, $\SI{0.25}{\milli\second}$, $\SI{0.5}{\milli\second}$, $\SI{1}{\milli\second}$
  \item[$\bullet$] Preheat temperature - $\SI{400}{\kelvin}$, $\SI{800}{\kelvin}$, $\SI{1000}{\kelvin}$, $\SI{1200}{\kelvin}$ 
\end{description}   
\Cref{fig:5_paramquali} shows the spatio-temporal variation of G and R as a function of different values of 4 different beam input parameters.

\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_paramquali.jpg}
\caption{Variation of temperature gradient (G) and liquid-solid interface velocity (R) of spot melt profile as a function of (a) preheat temperature, (b) spot ON time, (c) beam diameter, and (d) spot beam current. }
\label{fig:5_paramquali}
\end{figure}
From \Cref{fig:5_paramquali}, it can be concluded that increasing the preheat temperature, beam current, spot ON time and beam diameter moves the microstructure map towards the equiaxed region from the columnar region by reducing the temperature gradient. Increasing the energy input by increasing the beam power and ON time effectively moves the grain morphology towards the equiaxed region, which is in accordance with the results shown by Bontha et al. \cite{bontha2009effects}. The sensitivity of the temperature gradient (G) and the liquid-solid interface velocity (R) with respect to different parameters is shown in \Cref{fig:5_paramquali}. In order for robust understanding of the columnar to equiaxed transition (CET) during the solidification of the melt pool, it is important to quantify the influence of the beam input parameters on G and R.
 
\section{Quantitative Analysis}
\subsection{Theory}
An analytical model for the columnar to equiaxed transition in casting processes was developed by Hunt \cite{hunt1984steady} which provided relationship between the thermal gradient (G), interface velocity (R) and volume fraction of equiaxed grains ($\Phi$) formed during solidification. G$\ddot{a}$umann et al. \cite{gaumann2001single} extended the theory using Kurz-Giovanola-Trivedi (KGT) model for rapid solidification processes by neglecting the nucleation undercooling ($\Delta T_n$) at high thermal gradients ($\thicksim 10^6 K/m$). The underlying thermodynamics and kinetics of the columnar to equiaxed transition were simplified by G$\ddot{a}$umann et al. \cite{gaumann2001single} as formulated in \Cref{CET_eqn_5}.

\begin{equation}
\label{CET_eqn_5}
\left(\frac{G^n}{R}\right) = a{\left(\left(\frac{-4*\pi*N_o}{3 \ln(1-\Phi)}\right)^{\frac{1}{3}}{\frac{1}{n+1}}\right)}^n
\end{equation}

where \textbf{G} is the temperature gradient, \textbf{R} is the velocity of the liquid-solid interface, \textbf{$N_o$} is the nucleation density, \textbf{$\Phi$} is the volume fraction of equiaxed grains or probability of stray grain formation, \textbf{n} and \textbf{a} are the alloy constants. Nucleation density ($N_o$) depends on composition of the alloy and undercooling. Higher the value of $N_o$, higher the probability of formation of equiaxed grains during the solidification. Analytical calculation of the value of $N_o$ is complex and beyond the scope of this research work. In the literature \cite{vitek2005effect,anderson2010origin,gaumann2001single}, the value of  $N_o$ was experimentally calibrated and optimized. Different values are being reported for the same alloy system \cite{vitek2005effect,anderson2010origin,gaumann2001single}. Values of a, n and $N_o$ assumed in the results section of this paper are $1.25 x 10^6$, $3.4$ and $2 x 10^{15}$ respectively as reported by G$\ddot{a}$umann et al. \cite{gaumann2001single} and  Vitek \cite{vitek2005effect} for a similar nickel-base superalloy. By rearranging \Cref{CET_eqn_5} and applying the value for constants, the ratio ${G^n}/{R}$ can be directly correlated to the volume fraction of equiaxed or stray grains ($\Phi$) formed during the solidification of the melt pool \cite{raghavan2017corrigendum}.

\begin{equation}
\label{simpCET_eqn_5}
\Phi = 1 - exp\left({-2.36E19}*{\frac{R^{3/3.4}}{G^3}}\right)
\end{equation}

From \Cref{simpCET_eqn_5},it can be noted that, for a given material system, the value of $\Phi$ depends only on the values of $G$ and $R$.
\subsection{Calculation of Spatio-temporal $\Phi$}
Quantitative analysis can be done by calculating the volume fraction of equiaxed grains formed during solidification for different cases of simulated melt pool. The volume fraction of equiaxed grains, or the probability of stray grain formation ($\Phi$), can be calculated at every time step and at every nodal element of spatial domain of the liquid-solid interface using \Cref{simpCET_eqn_5}. The histogram plot in \Cref{fig:5_histo} shows the non-dimensional volume (frequency of nodal elements) vs. the probability of stray grain formation ($\Phi$). It can be observed from \Cref{fig:5_histo} that the volume fraction of equiaxed grains in the solidified melt pool can be changed from less than 10\% (\Cref{fig:5_histo}(a)) to more than 90\% (\Cref{fig:5_histo}(b)) by changing the beam input parameters. 
 
\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_histo.jpg}
\caption{Change in volume fraction of equiaxed grains ($\Phi$) or solidification morphology with respect to different input parameters.}
\label{fig:5_histo}
\end{figure}

For quantitative analysis of the effect of input parameters on the volume fraction of stray grains, design and analysis of experiments (DOX) \cite{montgomery2017design} approach was used. A full factorial model was developed with all the 4 input parameters with 2 levels for each of the factors ($2^k = 2^4$ simulations). To do the analysis, the response variable has to be a single value. So, volume weighted average was used to calculate the weighted average probability of stray grain formation in the entire melt pool. The volume fraction of equiaxed grains ($\Phi$) was used as the response variable in the design. Formulation to calculate the response variable is given in \Cref{weightphi_eqn_5}.

\begin{equation}
\label{weightphi_eqn_5}
\large{\Phi} = \frac{\Sigma v_i\phi_i}{\Sigma v_i}
\end{equation}

where $v_i$ is the volume of the discretized nodal element in the spatial domain (dx*dy*dz) and $\phi_i$ is the probability of stray grain formation at the corresponding nodal element calculated using \Cref{simpCET_eqn_5}.
 
\subsection{ANOVA}
The input variables and the parameter window within the practical limitations are listed in \Cref{spot_param_tab_ch5}

\begin{table}[h!]
\centering
 \begin{tabular}{||c|| c | c||} 
 \hline
 Parameter & Minimum & Maximum \\ [0.5ex] 
 \hline\hline
 Electron Beam Diameter $\left(\SI{}{\micro\meter}\right)$ & 200 & 800 \\ 
 
 Electron Beam Current $\left(\SI{}{\milli\ampere}\right)$ & 5 & 20 \\
 
 Spot ON time $\left(\SI{}{\milli\second}\right)$ & 0.1 & 1 \\
 
 Preheat Temperature $\left(\SI{}{\kelvin}\right)$ & 973 & 1528 \\ [1ex] 
 \hline
\end{tabular}
\caption{Values of input parameters of proposed scan strategy}
\label{spot_param_tab_ch5}
\end{table} 

Simulations were performed for all 16 possible combinations of parameters and weighted volume average of $\Phi$ was calculated for each of the simulations. The response variable ($\Phi$) calculated from the simulations for the statistical design is given in \Cref{tab_DOX_ch5}. 

\begin{table}[h!]
\centering
 \begin{tabular}{||C{1.75cm} | C{1.75cm} | C{1.75cm} | C{1.75cm} | C{2.5cm} | C{1.75cm}||} 
 \hline
 Case & A \linebreak Beam Diameter (\SI{}{\micro\metre}) & B \linebreak Beam Current (\SI{}{\milli\ampere}) & C \linebreak Beam On Time (\SI{}{\milli\second}) & D \linebreak Preheat Temperature (\SI{}{\kelvin}) & Response Variable \linebreak $\Phi$ (\%)\\ [0.5ex] 
 \hline\hline
 1 & 200 & 5 & 0.1 & 973 & 13.7 \\ 
 
 2 & 200 & 5 & 0.1 & 1528 & 57.5 \\ 
 
 3 & 200 & 5 & 1 & 973 & 15.9 \\ 
 
 4 & 200 & 5 & 1 & 1528 & 75.3 \\ 
  
 5 & 200 & 20 & 0.1 & 973 & 15.8 \\ 
 
 6 & 200 & 20 & 0.1 & 1528 & 67.8 \\ 
 
 7 & 200 & 20 & 1 & 973 & 20.6 \\ 
 
 8 & 200 & 20 & 1 & 1528 & 86.0 \\ 
 
 9 & 800 & 5 & 0.1 & 973 & 14.1 \\ 
 
 10 & 800 & 5 & 0.1 & 1528 & 58.8 \\ 
 
 11 & 800 & 5 & 1 & 973 & 20.4 \\ 
 
 12 & 800 & 5 & 1 & 1528 & 76.9 \\ 
 
 13 & 800 & 20 & 0.1 & 973 & 17.4 \\ 
 
 14 & 800 & 20 & 0.1 & 1528 & 68.2 \\ 
 
 15 & 800 & 20 & 1 & 973 & 22.9 \\ 
 
 16 & 800 & 20 & 1 & 1528 & 88.1 \\ [1ex] 
 \hline
\end{tabular}
\captionsetup{justification=centering}
\caption{$2^4$ simulation combinations and the corresponding volume fraction of equiaxed grains used in DOX}
\label{tab_DOX_ch5}
\end{table} 

Analysis of variance (ANOVA) approach was used to quantify the effect of each input parameter on the response variable ($\Phi$). The correlation between the input parameters and the response variable was studied in detail. 

\begin{table}[h!]
\centering
 \begin{tabular}{||C{3cm} | C{1.5cm} | C{1.75cm} | C{1.5cm} | C{1.5cm} | C{2.5cm}||} 
 \hline
 \textbf{Source} & \textbf{Sum of Squares} & \textbf{Degrees of Freedom} & \textbf{Mean Square} & \textbf{F-Value} & \textbf{p-value} \linebreak \textbf{Probability > F} \\ [0.5ex] 
 \hline\hline
 \textbf{Model} & 12904.19 & 5 & 2580.84 & 433.68 & < 0.0001 \\ 
 & & & & \\
 \textbf{A-Beam Diameter} & 12.60 & 1 & 12.60 & 2.12 & 0.1763 \\ 
 & & & & \\
 \textbf{B-Beam Current} & 183.6 & 1 & 183.6 & 30.85 & 0.0002 \\ 
 & & & & \\
 \textbf{C-Beam ON Time} & 538.4 & 1 & 538.4 & 90.45 & < 0.0001 \\ 
  & & & & \\
 \textbf{D-Preheat Temperature} & 11979.30 & 1 & 11979.30 & 2012.99 & < 0.0001 \\ 
 & & & & \\
 \textbf{C*D} & 190.44 & 1 & 190.44 & 32.00 & 0.0002 \\ [1ex] 
 \hline
\end{tabular}
\caption{Analysis of Variance (ANOVA) of the design}
\label{tab_ANOVA_ch5}
\end{table} 

Statistical significance of the individual parameters on the response variable was understood by analyzing the variance (ANOVA) of the design as shown in \Cref{tab_ANOVA_ch5}. The statistical validity of the model was confirmed with the adjusted R-squared value of the design greater than 98\%. The significance of each of the input parameters can be explained by their corresponding F-values listed in \Cref{tab_ANOVA_ch5}. The higher the F-value of the parameter, the greater it`s influence on the response variable ($\Phi$). Factors with F-value less than 4 are deemed statistically insignificant. The F-values of the parameters indicate that the beam diameter is insignificant (F-value=2.12) and that preheat temperature (F-value = 2012.99) is the most significant in affecting the volume fraction of equiaxed grains ($\Phi$) formed during the solidification. Quantitative relationship between the input parameters and the output response variable (volume fraction of equiaxed grains) was obtained using DOX approach which is formulated in \Cref{response_eqn_5}.

\begin{equation}
\label{response_eqn_5}
{\Phi\left(\%\right)} = -72.07 + 0.4517*B - 21.6595*C + 0.083408*D + 0.027628*CD
\end{equation}

where \textbf{B} is beam current (\SI{}{\milli\ampere}), \textbf{C} is beam ON time (\SI{}{\milli\second}) and \textbf{D} is the preheat temperature (\SI{}{\kelvin}). Local preheat temperatures can be maintained high by modifying melt strategies and keeping the entire layer close to the solidus temperature of the alloy. The design of scan strategy and experimental validation of simulation results is reported in the following section. 

\section{Experimental Validation}
\subsection{Scan Strategy and Fabrication}
To validate the modeling results, bulk samples of dimension 2 cm x 2 cm x 2 cm were fabricated with IN718 powder using Arcam\textsuperscript{\textregistered} S12 machine. As mentioned earlier, a novel spot melting strategy was developed and used instead of a standard raster melt pattern. The qualitative difference between spot and raster melting is explained in \Cref{fig:5_scan}. \Cref{fig:5_scan}(a) depicts a standard raster melt pattern in which the electron beam moves linearly to fill the space, as shown by the lines and arrows. \Cref{fig:5_scan}(b) depicts the spot melting pattern used to fabricate the samples to validate the model. In spot melting, the electron beam is turned on at a point with a specified current for a period of time (beam ON time), as indicated in \Cref{spot_param_tab_ch5}. Once the time period exceeds the defined beam ON time, the beam is moved to a new spot according to the sequence shown in \Cref{fig:5_scan}(b). Once the entire layer is filled with the independent spots (1-9), the next spot (10) is placed, overlapping the first spot, and the $11^{th}$ spot is placed beside the $2^{nd}$ spot, making it independent of the $10^{th}$ spot, and so on. The spot filling pattern shown in \Cref{fig:5_scan} (b) is purely qualitative. The number of spots per unit length and number of spots to skip in X and Y direction changes with respect to the fabricated samples. The spot filling continues in both the horizontal and vertical directions until the entire 2D layer (2 cm x 2 cm) is completely melted. Subsequently, the build platform is lowered and the melt pattern continues for the next 2D layer. Each layer is 50 microns thick and the samples consist of 400 layers (2 cm) in total.
\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_scan.jpg}
\caption{Qualitative comparison of (a) raster scan and (b) spot melting strategy}
\label{fig:5_scan}
\end{figure}

A total of 16 samples (\Cref{fig:5_fabsample}(a)) were fabricated with varying beam current (5-20 \SI{}{\milli\ampere}) and beam ON time (0.05-0.25 \SI{}{\milli\second}). \Cref{fig:5_fabsample}(b) shows the corresponding energy deposited (\SI{}{\kilo\joule}) per layer of the samples.

\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_fabsample.jpg}
\caption{(a) Top view of samples fabricated using spot melting strategy with varying beam current 
and beam ON time (b) Energy deposited (kJ) per layer of samples }
\label{fig:5_fabsample}
\end{figure}
\subsection{Insignificance of Beam Diameter}
The significance of beam diameter on grain morphology was experimentally analyzed by fabricating two samples using spot scan strategy (\Cref{fig:5_scan}(b)) with same beam current (\SI{20}{\milli\ampere}) and beam ON time (1 ms) but different beam diameters. The electron beam in Arcam\textsuperscript{\textregistered} is focused using electromagnetic coils, and changing the focus coil current controls the diameter of the beam. Even though the quantitative relationship between focus coil current and beam diameter is unknown, it can be qualitatively determined that larger coil current defocuses the beam, resulting in a larger beam diameter. Samples were fabricated with focus coil currents of \SI{0}{\milli\ampere} (highly focused - small beam diameter) and \SI{20}{\milli\ampere} (defocused - large beam diameter) within the practical limitation of the process. The samples were fabricated with lower energy deposition (\SI{1.9}{\kilo\joule}) per layer in order to decouple the effect of beam diameter from the preheat temperature. Energy deposition was controlled by reducing the number of spots in a layer. Samples were cut along the build direction (XZ plane) and the orientation of the grains were analyzed by studying the EBSD data using a scanning electron microscope. \Cref{fig:5_beamdia} shows the inverse pole figure along with the corresponding grain aspect ratio of the two samples. In agreement with the results from numerical simulations, no significant change in grain aspect ratio was observed between the samples. 
\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_beamdia.jpg}
\caption{EBSD: Inverse pole figure of samples (XZ plane) fabricated with beam current of \SI{20}{\milli\ampere}, beam ON time of \SI{1}{\milli\second} and focus offset coil current of (a) \SI{0}{\milli\ampere} (b) \SI{20}{\milli\ampere}. Grain aspect ratio of sample built with focus offset coil current of (c) \SI{0}{\milli\ampere} and (d) \SI{20}{\milli\ampere}}
\label{fig:5_beamdia}
\end{figure}
\subsection{Significance of Preheat Temperature}
\Cref{fig:5_meltpool} shows the top surface (XY plane) of samples 8 and 16 (in \Cref{fig:5_fabsample}(a)), that are fabricated using beam currents of \SI{10}{\milli\ampere} and \SI{20}{\milli\ampere}, respectively.  The beam ON time of \SI{0.25}{\milli\second} was kept the same for each spot. In the sample fabricated with the beam current of \SI{10}{\milli\ampere} (\Cref{fig:5_meltpool} (a)), it is important to note that the adjacent melt pools are distinct and their solidification are independent of each other. It can be logically inferred that the local preheat temperature of the substrate when electron beam hits spot \# 10 (see \Cref{fig:5_scan}(b)) is less than the solidus temperature of IN718 (\SI{1528}{\kelvin}). But in \Cref{fig:5_meltpool} (b), the adjacent melt pools are indistinguishable, which means melt pool \# 1 does not solidify completely before the electron beam hits spot \# 10 (see \Cref{fig:5_scan}(b)). This shows that the local preheat temperature of the substrate when electron beam hits spot \# 10 is greater than or equal to the solidus temperature of IN718 (\SI{1528}{\kelvin}). Local preheat temperature of the substrate in the sample 16 is maintained high by depositing more energy per layer (\Cref{fig:5_fabsample}(b)) which keeps the entire layer in molten state before the solidification begins.
\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_meltpool.jpg}
\caption{Melt pool of samples fabricated with beam current of (a) \SI{10}{\milli\ampere} (b) \SI{20}{\milli\ampere} with beam ON time of \SI{0.25}{\milli\second}}
\label{fig:5_meltpool}
\end{figure}

The samples 8 and 16 (\Cref{fig:5_fabsample}) were cut along the build direction (XZ plane) and electron back scattered diffraction (EBSD) imaging was done to determine the crystallographic orientation of the grains. \Cref{fig:5_preheat}(a) and (b) shows the inverse pole figures of samples 8 and 16 respectively. \Cref{fig:5_preheat}(c) and (d) shows the corresponding grain aspect ratio map. Directional (columnar) grain growth along the build direction was observed in the sample 8 with distinct melt pools (\Cref{fig:5_meltpool}(a)). Equiaxed grain growth was observed in the sample 16 with indistinguishable melt pools (\Cref{fig:5_meltpool}(b)). 
\begin{figure}[!]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{5_preheat.jpg}
\caption{EBSD: (a) Inverse pole figure of samples (XZ plane) fabricated with beam current of \SI{10}{\milli\ampere} and (b) \SI{20}{\milli\ampere} (c) Grain aspect ratio of sample built with \SI{10}{\milli\ampere} and (d) \SI{20}{\milli\ampere} with beam ON time of \SI{0.25}{\milli\second} }
\label{fig:5_preheat}
\end{figure}

These experimental results validate the modeling results that local preheat temperature is the most significant factor in columnar to equiaxed transition (CET) during the solidification in the electron beam AM process. Statistical evidence is provided by corresponding grain aspect ratio maps shown in \Cref{fig:5_preheat} 8(c) and (d). The sample fabricated with beam current of \SI{10}{\milli\ampere} has 60\% of grains with aspect ratio less than 0.25 while the sample fabricated with beam current of \SI{20}{\milli\ampere} has only 6\% of grains with aspect ratio less than 0.25.
\\[8pt]
The results in \Cref{fig:5_preheat} and \Cref{fig:5_beamdia} experimentally verify that changing beam diameter has an insignificant effect compared to changing the local preheat temperature of the substrate.

\section{Summary}
\begin{itemize}
\item Solidification texture of IN718 alloy was controlled during electron beam additive manufacturing by developing a novel melt scanning strategy optimized with a 3-D numerical model capable of predicting the fraction of equiaxed grain formation as a function of beam diameter, beam current, beam ON time, and preheat temperature.  The model relies on the spatial and temporal calculation of thermal gradient (G) and liquid-solid interface velocity (R).  
The results of the numerical calculations were then coupled with theoretical model for columnar to equiaxed transition during solidification.

\item Using design of experiments principles, numerical evaluations showed that the volume fraction of equiaxed grains ($\Phi$) increased, with an increase in beam-diameter, current and ON time, as well as, preheat temperature. The analysis of variance (ANOVA) approach was used to quantify the effect of all the input parameters on the volume fraction of equiaxed grains formed during the solidification of the simulated melt pool. A quantitative relationship was derived between the input parameters and the volume fraction of equiaxed grains formed. Preheat temperature is found to contribute the most in altering the volume fraction of equiaxed grains formed, and beam diameter is found to be the least significant among all the parameters considered. The influence of each of the beam input parameters on the columnar to equiaxed transition during the solidification was statistically explained.

\item The obtained results not only depend on the parameters of the new scan strategy but also rely on the geometry of the part being fabricated. The effect of build geometry on columnar to equiaxed transition is described in next chapter \Cref{ch:chapter_6}.
\end{itemize}

