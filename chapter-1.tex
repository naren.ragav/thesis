\chapter{Introduction}
\label{ch:chapter_1}

\section{Introduction to Additive Manufacturing Process}
Additive manufacturing (AM), commonly referred to as rapid prototyping, is the fabrication of 3-D parts by additively fusing one layer of raw material over the previously fused layers. An important advantage of AM over conventional processes is that it enables fabrication of complex structures which are either impossible or cost prohibitive to manufacture through traditional methods like milling and machining. New additive manufacturing (AM) technologies are revolutionizing the manufacturing sector with a significant reduction in manufacturing lead time, material wastage, and energy consumption. They are being adapted to fabricate critical components in aerospace, automobile, defense and medical industries\cite{wong2012review}. In aerospace sector, fabrication of functional parts using AM dramatically reduces the buy-to-fly ratio compared to conventional subtractive manufacturing processes like machining, where the desired shape and size of the part is obtained by removing the excess material.  Metal AM systems can be classified based on the type of raw material and energy source used in the process to melt the raw material. The majority of metal AM systems fall into two main classifications; powder bed fusion and directed energy deposition. For powder bed fusion systems the starting material is metal powder in contrast to directed energy technologies where the starting material may be either powder or wire. Lasers and electron beams are the two most commonly used heat sources to selectively melt the metal powder particles in the bed and fuse them to the underlying layers. Thin metal sheets are also used as raw materials and are bonded layer by layer using fast moving sonotrode. Classification of metal AM systems is shown in  \Cref{AM_Classification}. An electron beam additive process (Arcam\textsuperscript{\textregistered}) is the focus of this research work.

\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.75cm}
\includegraphics[width=6in]{1_AM_Classification.jpg}
\caption{Classification of metal AM systems}
\label{AM_Classification}
\end{figure}

\subsection{Business and Environmental Implications of Additive Manufacturing Technology}
Conventional manufacturing processes generally involve multiple steps to manufacture a part and number of steps may significantly increase with increase in the complexity of the design of the part. The complexity of the part is also limited by the ability of the conventional manufacturing processes. On the other hand, additive manufacturing is a single step process to manufacture a part however complex it may be. Additive manufacturing eliminates multiple steps involved in the conventional manufacturing process like machining. In the global market for manufacturing sector, it is important to remain competitive in terms of quality and cost. If wisely chosen, additive manufacturing will help in the reduction of overall product cost by reducing the material wastage, processing cost, energy cost, inventory cost and transportation cost involved during various stages of conventional manufacturing. 
\\[8pt]
Looking at the environmental perspective, AM helps in reducing the carbon footprint of a manufactured part. A case study \cite{guo2012feature} on a real world application of a titanium part in aerospace industry compared the amount of greenhouse gas emissions due to manufacturing of the part by AM and conventional manufacturing through milling. Using AM technology to manufacture that particular component, GHG emissions was estimated to be 49\% less compared to conventional manufacturing process. Once the engineering and metallurgical challenges in various additive manufacturing processes are addressed, AM is claimed to play vital role in reducing greenhouse emissions of next generation smart industries. \Cref{fig:1_AM_business} shows the results of detailed investigation \cite{nopparat2012resource} on comparison of energy consumption during various stages (raw material preparation phase and part fabrication phase ) by AM and injection molding (IM) of 500,000 units of a part. It can be observed that AM uses comparatively less energy during raw material phase and significantly more energy during production phase. It was concluded that for the given case, there is a threshold number of production units below which AM is advantageous and above which conventional IM is profitable in terms of energy and economy. Apart from addressing current engineering challenges in the field additive manufacturing, it is important to study the various other factors involved including market demand situation, inventory costs and other related factors in the entire production chain of a particular component in order for successful production scale implementation of AM to manufacture that component.


\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.75cm}
\includegraphics[width=6in]{1_AM_business.jpg}   
\caption{Energy Usage Comparison between AM and IM \cite{nopparat2012resource}}
\label{fig:1_AM_business}
\end{figure}


During the early stages, additive manufacturing was used to make prototypes, models and for tooling applications. In recent years, it has found the way into large scale manufacturing sector, particularly in aerospace and medical implant industries. Aerospace and medical implant industries are showing great interest in additive manufacturing due to the advantage of reduction in material wastage of expensive raw materials, reduction in manufacturing lead-time and design freedom. In aerospace industries, reduction in material wastage of expensive raw materials like titanium and nickel-base alloys will reduce the buy-to-fly ratio of the components. Design freedom allows the aerospace industry to replace the heavy weight conventional design of the parts with low weight optimized design.  
\subsubsection{Industrialization of Metal AM}
GE Aviation is one of the pioneers in transforming additive manufacturing from prototype level process to pilot-line production scale process. GE Aviation is considering utilizing the laser sintering process to additively manufacture turbine blades with titanium aluminide for next generation engine GE9X for the new Boeing 777X. According to GE, GE9X is the most advanced fuel-efficient commercial aircraft engine ever built. Currently, GE Aviation is additively manufacturing fuel nozzle using direct metal laser melting (DMLM) process for its upcoming LEAP engine with each engine requiring 19 fuel nozzles in its combustion section. With more than 6700 orders till now from 20 countries, adding up to nearly $\$96$ billion (U.S. list price), the LEAP is GE Aviation's best-selling engine in its history.
According to a recent report from GE \cite{toddGE}, benefits of the new additively manufactured fuel nozzle compared to the previous generation are
\begin{description}
  \item[$\bullet$] Consolidation of 20 parts into one additively manufactured component
  \item[$\bullet$] 5x life improvement of overall fuel delivery system
  \item[$\bullet$] 25\% reduction in weight of the part
\end{description}

\Cref{fig:1_genozzle} shows the above mentioned fuel nozzle and also the world`s first additively manufactured component to fly in a commercial jet engine. 

\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.75cm}
\includegraphics[scale=0.3]{1_genozzle.jpg}   
\caption{Production scale additively manufactured fuel nozzle from GE Aviation \cite{genozzle}}
\label{fig:1_genozzle}
\end{figure}


\subsection{A Brief Introduction to Arcam\textsuperscript{\textregistered} process}
In an electron beam additive system, fine metal powders (\SI{30}{\micro\metre}-\SI{100}{\micro\metre}) stored in the powder hopper are raked over the start plate using a metallic brush. The layer thickness is usually in the range of \SI{50}{\micro\metre} to \SI{100}{\micro\metre}. The entire layer of powder is preheated and sintered using diffused electron beam to avoid surface charge accumulation on the powder particles and 'smoking'\cite{cordero2017powder} during melting. The powder particles are then melted using the electron beam according to the 2D design of the particular layer and fused with the previous layers. The build platform is then lowered and new layer of powder is raked and melted. This process continues until melting of all the layers of the part is complete. The component is then taken out of the build chamber and powder recovery system is used to recover the unmelted powder particles. \Cref{fig:arcam_steps} shows the processing steps involved in the Arcam\textsuperscript{\textregistered} process.

\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.75cm}
\includegraphics[width=6in]{Arcam_Overview.pdf}   
\caption{Processing steps in Arcam\textsuperscript{\textregistered}}
\label{fig:arcam_steps}
\end{figure}

Electron beam based processes have significantly higher power density \cite{gajapathi2011controlling} than laser based processes, and also, the electron beam based Arcam\textsuperscript{\textregistered} process has comparatively faster part fabrication rate than laser based processes. The difference in part fabrication rate is attributed to difference in beam deflection mechanisms. Mechanical inertia experienced by the deflecting mirrors limits the travel speed of the laser beam while electron beam can travel much faster (\SI{8000}{\meter\per\sec})\cite{Multibeam1850} owing to the use of electromagnetic deflection coils. Parts fabricated using the electron beam process also tend to have significantly lower residual stresses \cite{sochalski2015comparison} compared to parts manufactured using laser based fabrication processes. Even though AM is a layer by layer fabrication technique, the underlying physics is the same as welding, but for the complex boundary conditions and type of raw material typical to each AM process. \Cref{fig:1_arcam} shows the schematic of an Arcam\textsuperscript{\textregistered} machine.


\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.75cm}
\includegraphics[scale=0.7]{1_arcam.jpg}   
\caption{Schematic of an Arcam\textsuperscript{\textregistered} machine and corresponding picture of the build chamber  \cite{arcamschematic}}
\label{fig:1_arcam}
\end{figure}


\subsection{Alloys used in powder bed AM processes}
Aerospace and medical implant industries are couple of the major sectors adapting additive manufacturing for production scale. In addition to design freedom, to extract the economical advantage of AM process through reduction of the buy-to-fly ratio, the raw material used for fabrication have to expensive. Couple of the most commonly used alloys that are expensive in aerospace and medical implant industries are nickel-base superalloys and titanium alloys.
\\[8pt]
In general, nickel-base superalloys constitute about 40-50\% of the the total weight of the aircraft engine \cite{pollock2006nickel}. In addition to corrosion resistance at high temperature, they exhibit high strength, toughness, and creep resistance at high temperatures. Some of the nickel alloys can withstand temperatures upto 1200\textdegree C \cite{pollock2006nickel} ($\thicksim 90\%$ of the melting point). 
Due to this, the nickel alloys are used in hot section of a turbine engine. The high temperature properties are owed to precipitation strengthening ($\gamma'$-$Ni_3Al$ and $\gamma"$ - $Ni_3Nb$) of the alloy system. Inconel 625 (solid solution strengthened) and Inconel 718 ($\gamma"$ strengthened) are couple of the most commonly and successfully used nickel-based superalloys in AM processes. AM fabrication of high $\gamma'$ nickel-alloys are challenging because of it`s non-weldability owing to their sensitivity to cracking. Numerous research works \cite{ramsperger2016microstructure, carter2014influence} are trying to address the challenges for successful fabrication of non-weldable nickel-base alloys using AM. 
\\[8pt]
On the other hand, due to their high strength to weight ratio, titanium alloys are used in cold section of turbine engine (low pressure compressor section) and for structural applications in replacing low alloy steel and aluminum alloys \cite{Tioverview}. Due to it`s excellent bio-compatibility, titanium alloys are extensively used in dental and orthopedic implants \cite{brunette2012titanium, jardini2014cranial, murr2010next}. Ability of fabricating patient specific designs and complex mesh structures has enabled the foray of AM in this field. Among titanium alloys, Ti6Al4V is the most commonly used in metal AM processes. There has been considerable research and development activities on replacing some heavier nickel-base superalloy components with lighter titianium aluminide alloy ($\gamma$-Ti-Al-based alloy) for high temperature applications \cite{dimiduk1999gamma, kim1989intermetallic}. Some of the recent studies \cite{murr2010characterization, tang2015additive} have evaluated the feasibility of fabricating $\gamma$-Ti-Al-based alloys using additive manufacturing.  

\section{Factors Affecting Mechanical Performance of Engineering Components}
\bigskip Lifetime and performance of any fabricated component depend on its mechanical properties. In general, the mechanical properties (fatigue, yield strength, ultimate tensile strength, ductility, and creep) of an engineering component are primarily dictated by (a) design geometry, (b) surface roughness, and (c) microstructure of the component. Usually, the geometry and surface roughness play a role during crack initiation, while crack propagation is predominantly dictated by the underlying microstructure. In this process-structure-property-performance (PSPP) linkage, understanding the process-structure relationship is important to obtain desired mechanical performance of the part fabricated through AM.
\\[8pt]
The microstructure of the fabricated component can be controlled by understanding the (a) liquid-solid and (b) solid-solid phase transformations of the alloy system with respect to complex thermal histories, which is in turn affected by the processing conditions. In the electron beam additive process (Arcam\textsuperscript{\textregistered}) discussed in this work, depending on the material system and processing conditions, both types of phase transformations can occur simultaneously in different locations depending upon spatiotemporal variations of temperature. Depending on the alloying elements and processing conditions, an alloy can have different crystal structures (FCC, BCC, HCP, etc.,) and multiple crystal structures can coexist at the final stage which dictates the ductility, tensile and other mechanical properties of the alloy system. Formation of precipitates is also dictated during the solid-solid phase transition. Understanding this phenomenon helps in pinning the dislocation movement thereby increasing the lifetime of the material. This phenomenon is well understood and Time-Temperature-Transformation (TTT) diagram is available in literature \cite{ahmed1998phase, xie2005ttt, oradei1991current} for most commonly used alloy systems. In addition to JMAK and STK models, software like Thermo-Calc can be used to understand the solid-solid phase transitions with respect to the alloy compositions.
\\[8pt]
The primary focus of this research work is to understand the process-strucutre relationship by analyzing the transient behavior of liquid-solid phase transformation (solidification) as a function of process parameters.




