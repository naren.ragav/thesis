\chapter{Overview of Numerical Modeling Framework}
\label{ch:chapter_3}

\section{Truchas}
Truchas \cite{korzekwa2009truchas} is an open-source, highly parallel, multi-physics continuum scale code developed as a part of Advanced Simulation and Computing (ASC) project (Telluride) at Los Alamos National Laboratory (LANL). Primary motivation for the development of the code was to understand the transient metal casting process for nuclear application. Truchas is primarily written in Fortran with a small fraction of the software written in C and Python. Appropriate user-subroutines have been developed to design and implement melt scan strategies (moving heat source) in Truchas to simulate the transient rapid solidification occurring during metal powder bed additive manufacturing processes.   

\subsection{Conservation Equations}
Multi-physics aspect of Truchas includes heat transfer and phase change, fluid dynamics including Marangoni flow due to surface tension gradient, electromagnetics and solid mechanics. The simulations and results in \Cref{ch:chapter_4} and \Cref{ch:chapter_5} considers only the physics of heat conduction and phase change. Conservation equation for heat conduction and phase change is given by \Cref{conserv_eqn_ch3}. 

\begin{equation}
\label{conserv_eqn_ch3}
\frac{\partial {\left(\rho h\right)}}{\partial t} = \bigtriangledown . \left(k\bigtriangledown T\left(h\right)\right) + \dot{S}
\end{equation} 

where $\rho$ is the density, $k$ is the the thermal conductivity, $h$ is the specific enthalpy and $\dot{S}$ is the source term. Source term includes both positive and negative fluxes. Some of the source terms considered for the additive manufacturing problem are 

\begin{description}
  \item[$\bullet$] Heat source used to melt the powder particles (electron beam or laser).
  \item[$\bullet$] Latent heat evolution during phase change.
  \item[$\bullet$] Radiative and convective heat fluxes at the boundaries.
\end{description}     


\Cref{conserv_eqn_ch3} contains the diffusive flux, the advective flux is described in \Cref{ch:chapter_7}. The partial differential equation is then discretized by mimetic finite difference method. The resulting set of non-linear equations are then solved using one of Jacobian Free Newton Krylov (JFNK) or Accelerated Inexact Newton(AIN) methods.   


The effect of fluid mechanics on solidification is explained in \Cref{ch:chapter_7}. The physics involved, conservation equation and solution procedure are discussed in \Cref{ch:chapter_7}.

\subsection{Heat Source Profile}
To improve the accuracy of the prediction of the numerical simulations, it is important to understand the effect of interaction of the heat source on the material being fabricated. One of the important parameters of the interaction is the distribution profile of heat source. An electron beam additive manufacturing system (Arcam\textsuperscript{\textregistered}) is the focus of this research. In general, laser interacts only with the surface of the material while electron beam penetrates into the impinging material. In addition to the surface heat source profile, there will be a heat source profile along the depth of the material as well. The surface profile is given by Gaussian distribution as given in \Cref{Ixy_eqn_ch3}.

\begin{equation}
\label{Ixy_eqn_ch3}
I_{xy} = \frac{1}{2*\pi*{\sigma}^2}exp\left({\frac{-\left(x^2+y^2\right)}{2*\sigma^2}}\right)
\end{equation} 

where $I_{xy}$ is the surface intensity profile ,$\sigma$ is the standard deviation, $x$ and $y$ are the distance from the center of the beam in X and Y axis respectively. Beam diameter is directly related to $\sigma$.\\[8pt]


The vertical intensity profile is given by an equation derived from experimental measurement. Absorbed power per unit volume and penetration depth has been approximated and the quadratic approximation of the vertical intensity profile is given in \Cref{Iz_eqn_ch3}\cite{zah2010modelling} 

\begin{equation}
\label{Iz_eqn_ch3}
I_{z} = -{3}{\left(\frac{z^2}{{z_e}^2}\right)}+2\frac{z}{z_e} + 1
\end{equation} 

where $I_z$ is the heat source intensity profile along z-direction, $z$ is the distance (depth) from the surface of the sample, $z_e$ is the absolute penetration depth. Absolute penetration depth is defined as the depth at which 99\% of the electron beam power is absorbed. Absolute penetration depth is given by \Cref{ze_eqn_ch3} \cite{schiller1982electron, qin2004temperature}

\begin{equation}
\label{ze_eqn_ch3}
z_e = 21*\left(\frac{V_e^2}{\rho}\right)
\end{equation} 

where $z_e$ is the penetration depth in \SI{}{\micro\meter}, $V_e$ is the accelerating voltage of the electron beam in $kV$, $\rho$ is the density of the material in $\frac{kg}{m^3}$. \\[8pt]


Volumetric profile of the electron beam heat source is given by 

\begin{equation}
\label{vol_eqn_ch3}
\dot{Q}\left(x,y,z\right) = -\eta_e*\eta_b*{Q_{max}}*\frac{I_xy*I_z}{z_e}
\end{equation} 

where
\begin{equation}
\label{qmax_eqn_ch3}
Q_{max} = I_e*V_e 
\end{equation} 

where $I_e$ is the electron beam current, $V_e$ is the electron beam voltage, $\eta_e$ is the surface energy conversion and $\eta_b$ is the beam control efficiency. In general, beam control efficiency and surface energy conversion are high $ \left( \thicksim 0.9 \right) $ for most of the materials interacting with electron beam systems \cite{messler2008principles}. Combining \Cref{Ixy_eqn_ch3}, \Cref{Iz_eqn_ch3}, \Cref{vol_eqn_ch3}, following heat source profile can be defined.

\begin{equation}
\label{final_eqn_ch3}
\dot{Q}\left(x,y,z\right) = -\eta_e*\eta_b*{I_e*V_e}*\frac{4*ln(0.1)}{\pi*d^2*z_e}exp\left({\frac{4ln\left(0.1\right)*\left({x^2+y^2}\right)}{d^2}}\right)*\left(-{3}{\left(\frac{z^2}{{z_e}^2}\right)}+2\frac{z}{z_e} + 1\right)
\end{equation} 

where $d$ is the electron beam diameter. In Arcam\textsuperscript{\textregistered}, the beam voltage ($V_e$) is constant (60 kV) and beam current ($I_e$) is variable.

\subsection{Limitations and Assumptions of the model}
The simulations are completely continuum scale. The interaction between the powder particles and electron beam is ignored. Preheating (sintering) phase of the process is ignored. Selective evaporation of elements in the melt pool has been reported \cite{nandwana2016recyclability} in AM process. Energy loss due to evaporation cooling is not considered in this model. Expansion and shrinkage of the material due to the changes in temperature during melting and solidification are not accounted for in these simulations. The mass is conserved within each cell, and cell volumes are kept constant before, during and after the phase change. Hence, the density of the material is assumed to be constant for all the phases of materials used in the simulation.

\subsection{Initial and Boundary Conditions}
Local preheat temperature of the substrate, one of the indirect control parameters in Arcam\textsuperscript{\textregistered} process, was used as the initial condition of the spatial domain, and was considered a variable in the simulations. The top surface of the build radiates heat to the surroundings. Part fabrication in the Arcam\textsuperscript{\textregistered} process takes place in a low pressure environment ($10^{-3}$ mbar partial pressure of helium) \cite{Arcam}, therefore the heat transfer due to convection on the top surface of the domain is negligible and was assumed to be zero in the simulations. Hence the boundary condition at the top surface ($\gamma$) was formulated by applying energy conservation \Cref{rad_eqn_ch3}

\begin{equation}
\label{rad_eqn_ch3}
-k\Delta T = \epsilon*\sigma*\left(T_x^4 - T_a^4\right) \hspace{5em} x\hspace{0.5em}\epsilon\hspace{0.5em} \gamma
\end{equation} 

where $T_a$ is the ambient temperature ($25$\textdegree $C$), $k$ is the thermal conductivity of the material \Cref{prop_tab_ch3}, $\epsilon$ is the emissivity ($0.4$ \cite{keller2015total}) and $\sigma$ is the Stefan-Boltzmann constant ($5.67$ x $10^{-8} W.m.K^{-4}$).\\[8pt]


During the fabrication process, the part is encapsulated by fine metal powder particles of varying diameters $\left(\SI{20}{\micro\meter}-\SI{100}{\micro\meter}\right)$. Alkahari et al \cite{alkahari2012thermal} experimentally measured and theoretically verified the thermal conductivity of metal powders of varying diameters $\left(\SI{10}{\micro\meter}-\SI{100}{\micro\meter}\right)$ used in selective laser melting. The thermal conductivity of metal powders was found to be between $0.33 - 1.5\%$ of the thermal conductivity of bulk metals. The thermal conductivity of metal powders is so low such that it can be considered an insulator for the short duration (\SI{}{\milli\second}) of simulations. Hence the boundary conditions on the remaining five faces were assumed to be adiabatic. That is, a Neumann boundary condition with zero flux across the surfaces.

\subsection{Geometry and Meshing}
Since the focus of this research is to understand the transient conditions in bulk geometry of the printed part, the physical domain is assumed to be a thick plate. The maximum size of the domain simulated was 5 x 5 x 2 mm. The geometry is created and meshed using CUBIT\cite{blacker1994cubit}. Depending on the beam diameter, the mesh size was changed between \SI{5}{\micro\meter} and \SI{20}{\micro\meter}. Template script for generating the mesh using CUBIT command line can be found in \Cref{App:CUBIT_code}.

\subsection{Scaling of the Code}
Truchas is developed to run on high performance clusters with distributed memory systems. Systems in which the CPU`s have their own memory is called distributed memory architecture opposed to shared memory architecture where the a common memory is shared by all the CPUs. Truchas uses MPI library (Message Passing Interface) along with PGSLib to communicate between the processors. Depending on the size of the problem, the maximum number of spatial domains used in the simulations is about 3 million. Truchas is a parallel code capable of employed in HPC clusters for efficient simulations. Depending on the number of processors used, the spatial domain (mesh) is discretized into numerous sub-domains during the initializing phase of the simulation using Chaco partition library.
\\[8pt]
Scalability analysis was done to understand the effectiveness of the code in clusters. 2 types of scaling analysis can be done (a) weak scaling and (b) strong scaling. During weak scaling analysis, load per core or processor remains constant (number of nodes/equations in the mesh allocated per core/processor is constant) but number of workers/processors is increased. In strong scaling, the total load (number of nodes/equations in the mesh allocated to the cluster is constant) is kept constant but the number of processors is increased. 
\\[8pt]
Strong scaling analysis is done for pure heat transfer and phase change problem in Truchas. Total number of elements in the mesh is kept constant at 2 million. Total number of time steps in the simulation is constant at 2068. Number of cores used to solve the problem has been increased from $2^3$ to $2^{10}$. This analysis was done on a Cray XK7 machine with 16 core AMD interlagos and 32GB of RAM per node (cluster name: CADES, location: ORNL). 
\\[8pt]
Absolute strong scaling efficiency of a problem size $N$ on $P$ processors is defined in \Cref{scaling_eqn_ch2} \cite{keyes1998scalable}. 

\begin{equation}
\label{scaling_eqn_ch2}
\eta\left(N,P\right) = \frac{1}{P} \frac{T\left(N,1\right)}{T\left(N,P\right)}
\end{equation} 

where $T(N,1)$ is the total wall time of the simulation of size $N$ using 1 processor and  $T(N,P)$ is total wall time of the simulation using $P$ processors. Since the current problem size (2 million elements) is too large for 1 processor, minimum number of processors used in this case is 16. Hence, the relative scaling efficiency of scaling from $Q$ processors to $P$ processors is defined in \Cref{newscaling_eqn_ch2} \cite{keyes1998scalable}. 

\begin{equation}
\label{newscaling_eqn_ch2}
\eta\left(N,P|Q\right) = \frac{Q}{P} \frac{T\left(N,Q\right)}{T\left(N,P\right)}
\end{equation} 


Speed up ratio is another quantity to measure the performance of the code. Speed up of the code is defined as the ratio of time taken for the code to run on 1 core to time taken on N cores. \Cref{scaling_tab_ch3} shows the detailed usage of wall clock time by each part of the code during execution for the specified fixed problem size (strong scaling).  

\begin{table}[!h]
\centering
 \begin{tabular}{||C{1.75cm} | C{2cm} | C{2cm} | C{1.75cm} | C{1.75cm} | C{1.75cm} | C{1.75cm} ||} 
 \hline
 Total Number of Cores & Elements assigned per core & Total Wall Time (s) & Reading Input (s) & Initialization(s)& Diffusion Solver (s) & Writing Output (s)\\ [0.5ex] 
 \hline\hline
 16 & 125,000 & 15640 & 0.104 & 1934 & 13060 & 449   \\ 
 32 & 62,500 & 8510 & 0.140 & 1551 & 6599 & 265.7  \\ 
 64 & 31,250 & 4915 & 0.145 & 1394 & 3305 & 171.8   \\ 
 128 & 15625 & 3261 & 0.150 & 1400 & 1732 & 117.6  \\ 
 256 & 7813 & 2420 & 0.161 & 1401 & 906.8 & 106.2   \\ 
 512 & 3906 & 1955 & 0.1548 & 1377 & 472.3 & 101.4  \\ 
 1024 & 1953 & 1816 & 0.2947 & 1460 & 276 & 77.26  \\  [1ex]
 \hline
\end{tabular}
\caption{Strong Scaling: Wall Time Usage Analysis}
\label{scaling_tab_ch3}
\end{table} 


\Cref{fig:2_scaling}(a) shows the speed up ratio and \Cref{fig:2_scaling}(b) shows the comparison of relative scaling efficiency with data from 16 processors as the baseline calculated from the data in \Cref{scaling_tab_ch3}. From \Cref{fig:2_scaling} (b), it can be concluded that for this physics and defined problem size, increasing the number of processors to 1024 is essentially a waste of the computational resource. It can also be concluded that for efficient use of the computational resource, the load assigned should be between 30k and 15k elements per processor approximately. Care should be taken that this scaling analysis will not be valid if new physics is added to the model.    

\begin{figure}
\centering
\captionsetup{justification=centering,margin=1.25cm}
\includegraphics[width=6in]{2_scaling.jpg}
\caption{(a) Speed up ratio and (b) Scaling Efficiency of Truchas (Heat transfer and phase change)}
\label{fig:2_scaling}
\end{figure}

\subsection{Material Properties}
Thermophysical properties of IN718 used in the simulations are given in \Cref{prop_tab_ch3}

\begin{table}
\centering
 \begin{tabular}{||l | c | c||} 
 \hline
 Property & Value & Unit \\ [0.5ex] 
 \hline\hline
Density & 7451 & $kg.m^{-3}$ \\ 
 
 Solidus Temperature & 1528 & K \\ 

 Liquidus Temperature & 1610 & K \\ 

 Latent Heat of Fusion & 227000 & $J.kg^{-1}$ \\ 

 Specific heat capacity of solid & 600 & $J.kg^{-1}.K^{-1}$ \\ 
 
 Specific heat capacity of liquid & 775 & $J.kg^{-1}.K^{-1}$ \\
 
 Thermal conductivity of solid @ 1300 K & 26.6 & $W.m^{-1}.K^{-1}$ \\
 
 Thermal conductivity of liquid @ 1850 K & 29 & $W.m^{-1}.K^{-1}$ \\ [1ex] 
 \hline
\end{tabular}
\caption{Thermophysical properties of IN718 \cite{kamnis2008mathematical, pottlacher2002thermophysical}}
\label{prop_tab_ch3}
\end{table} 

\subsection{Calculation of G and R}
\label{subsec:GR_calc_ch3}
As explained in \Cref{ch:chapter_2}, the solidification microstructure is primarily dictated by thermal gradient ($G$) at the liquid-solid interface and velocity ($R$) of the liquid-solid interface. The output of Truchas is spatio-temporal variation of temperature. The values of resultant $G$ is extracted from the temperature data based on \Cref{G_eqn_ch3}. 

\begin{equation}
\label{G_eqn_ch3}
G = \sqrt[2]{{G_x}^2+{G_y}^2+{G_z}^2}
\end{equation} 

where $G_x$, $G_y$ and $G_z$ are thermal gradient along X, Y and Z directions respectively as shown in \Cref{Gx_eqn_ch3}. 

\begin{equation}
\label{Gx_eqn_ch3}
G_x = \frac{dT}{dx}   ;  G_y = \frac{dT}{dy}   ;   G_z = \frac{dT}{dz}
\end{equation} 


The cooling rate is calculated according to \Cref{GR_eqn_ch3}.
\begin{equation}
\label{GR_eqn_ch3}
Cooling Rate = |\frac{dT}{dt}|
\end{equation} 
The cooling rate is negative during melting and positive during solidification. This will help in identifying whether particular mesh element is melting or solidifying.  

The liquid-solid interface velocity can now be calculated as the ratio of cooling rate and resultant thermal gradient as shown in \Cref{R_eqn_ch3}\cite{rappaz1989modelling}.
\begin{equation}
\label{R_eqn_ch3}
R = \frac{|\frac{dT}{dt}|}{\sqrt[2]{{G_x}^2+{G_y}^2+{G_z}^2}}
\end{equation} 

To demonstrate the calculation of G and R and their distribution, a single spot simulation is performed. \Cref{fig:3_interfacetrack} shows the liquid-solid interface tracking along XZ symmetrical plane (side view) of the melt pool solidification with plots of liquidus (1610K) isotherm of the melt pool at different time of a simulation with beam current of \SI{20}{\milli\ampere} turned ON for \SI{1}{\milli\second} with a preheat temperature of \SI{1528}{\kelvin} (solidus). From \Cref{fig:3_interfacetrack}, it is evident that the melting and expansion of the melt pool continues even after the beam is turned off at \SI{1}{\milli\second}. In this case, the melt pool continues to expand and solidification begins only at \SI{10}{\milli\second} as shown in \Cref{fig:3_interfacetrack} (a) and completely solidifies at \SI{30}{\milli\second} as shown in \Cref{fig:3_interfacetrack} (d).

 \begin{figure}
   \centering
   \captionsetup{justification=centering,margin=1.75cm}
   \includegraphics{3_interfacetrack}   
   \caption{Transient Liquid-Solid interface tracking of melt pool (a) at the beginning of solidification 10ms (b) 20 ms, (c) 26 ms, and (d) end of solidification 30 ms.}
   \label{fig:3_interfacetrack}
  \end{figure}
\Cref{fig:3_OverlapGradient} shows the overlap of \Cref{fig:3_interfacetrack} (a), i.e. the liquidus isotherm at the beginning of the solidification, and temperature gradient vector of the entire domain. The temperature gradient vectors in the melt pool point towards the center of the heat source where the temperature is higher. The direction of the thermal gradient vector is normal to the liquid-solid interface along the liquidus isotherm which essentially dictates the direction of crystal growth at the liquid-solid interface.

 \begin{figure}
   \centering
   \captionsetup{justification=centering,margin=1.75cm}
   \includegraphics[scale=0.15]{3_OverlapGradient}   
   \caption{Overlap of Thermal Gradient vector and Liquidus isotherm (1610 K) along the XZ plane.}
   \label{fig:3_OverlapGradient}
  \end{figure}

Spatio-temporal distribution of $G$ and $R$ are then calculated according to \Cref{G_eqn_ch3} and \Cref{R_eqn_ch3}. The values are calculated at all of the elements in the spatial domain of the simulation. The values have to be filtered only along the elements at the liquid-solid interface. This extraction is automated and performed using a python script coupled with an open-source, parallel, data visualization software VisIt \cite{visit2003visit}. The script is given in \Cref{App:GR_python}. The values along the liquidus isotherm (\SI{1610}{\kelvin}) of the melt pool are used to obtain spatial and temporal distributions of the G and R only at the liquid-solid interface as the melt pool solidifies. These distributions are then used to predict the transition from columnar to equiaxed microstructure. The extracted G and R values are plotted on the reference solidification map \cite{nastac2001advances} for IN718. 


 \begin{figure}
   \centering
   \captionsetup{justification=centering,margin=1.75cm}
   \includegraphics[scale=0.2]{3_GRVariation}   
   \caption{Variation of G and R as a function of solidification time}
   \label{fig:3_GRVariation}
  \end{figure}


\Cref{fig:3_GRVariation} shows the variation of temperature gradient (G) and liquid-solid interface velocity (R) as the melt pool collapses. From \Cref{fig:3_GRVariation}, it is evident that the temperature gradient is high and the liquid-solid interface velocity is low as the melt pool begins to solidify. As the liquid front advances and melt pool shrinks, the temperature gradient decreases and the liquid-solid interface velocity increases rapidly. This behavior of G and R with respect to solidification time agrees with the results for spot welding by Debroy et al. \cite{he2003heat, he2005heat, rai2008heat, zhang2003modeling}. Quantitative and qualitative analysis of this spatio-temporal variation and it`s effect on solidification microstrucutre in electron beam additive manufacturing is explained in the following chapters.