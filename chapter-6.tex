\chapter{Geometry Dependency of CET and Infrared In-situ Monitoring}
\label{ch:chapter_6}
\section{Effect of Build Geometry on CET}
The obtained result from previous chapter is extended to study the effect of the process parameters of the scan strategy on build geometry. The scan strategy was explained qualitatively in the previous chapter \Cref{fig:5_scan} (b).
\\[8pt]
Initially, the 3D CAD geometry is sliced into numerous thin layers with respect to the layer thickness. Then, the corresponding images of all the 2D layers are pixelated. Each pixel corresponds to a spot to be melted. The linear pixel density (number of pixels per unit length) is one of the variables during this step. The pixel density is kept constant for a given part. Each pixel corresponds to one melt spot during fabrication as shown in \Cref{fig:6_spot}. Another parameter in the spot melt strategy is the number of pixels to skip between two consecutive spot melts (pixels skipped between \#1 and \#2 in \Cref{fig:6_spot}). Hence, energy deposition per layer per part can be varied by varying (a) pixel density and/or (b) beam ON time at a spot and/or (c) beam current. 
\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=4in]{6_spot.jpg}
\caption{Qualitative spot melting strategy.}
\label{fig:6_spot}
\end{figure}
\\[8pt]
\subsection{Scan Strategy and Parameters}
In this experiment, 6 samples were fabricated with the spot melting using an Arcam\textsuperscript{\textregistered} machine. From \Cref{fig:5_fabsample} and \Cref{fig:5_meltpool}, it can be observed that increasing the energy density resulted in the transition from columnar to equiaxed morphology. In this study, the beam ON time \SI{0.25}{\milli\second} and beam current \SI{20}{\milli\ampere} were kept constant for all the samples. The energy deposited per layer was changed from one part to the other by varying the linear pixel density. \Cref{tab_ch6} lists the dimensions of the parts, linear pixel density and associated energy density. Energy density is calculated as per \Cref{energy_eqn_6}.
\begin{equation}
\label{energy_eqn_6}
Energy Density = \frac{N_p*I*V}{A}
\end{equation}
where $N_p$ is the number of pixels per part per layer, $I$ is the beam current, $V$ is the beam voltage, $A$ is the cross sectional area of part. 

\begin{table}[h!]
\centering
 \begin{tabular}{||C{1.75cm} | C{2.75cm} | C{1.75cm} | C{2.75cm} |  C{1.75cm}||} 
 \hline
 Part \# & Dimensions \linebreak (L x W x H)(mm) & Linear pixel Density \linebreak (pixels per mm) & Total pixels per layer & Energy Density \linebreak (MJ/$m^2$)\\ [0.5ex] 
 \hline\hline
 1 & 20x20x20 & 4 & 6400 & 4.8 \\ 
 
 2 & 20x20x20 & 5 & 10000 & 7.5 \\ 
 
 3 & 20x20x20 & 6 & 14400 & 10.8 \\ 
 
 4 & 20x20x20 & 7 & 19600 & 14.7 \\ 
  
 5 & 40x40x20 & 5 & 40000 & 7.5 \\ 
 
 6 & 40x40x20 & 7 & 78400 & 14.7 \\ [1ex] 
 \hline
\end{tabular}
\caption{Spot  melt parameter list to study the effect of geometry on CET}
\label{tab_ch6}
\end{table}
   
All the parts are \SI{20}{\milli\meter} tall. 4 parts were fabricated with cross section of 20x20mm and 2 parts were fabricated with a cross section of 40x40mm. The linear pixel density was increased from 4px/mm to 7 px/mm from part 1 to part 4. It is important to note that part 2 and part 5 has same pixel density (energy density) but different cross section. Similarly, part 4 and part 6 has the same energy density but different cross section. 
 
\Cref{fig:6_buildschematic}(a) shows the part geometry and the energy density associated. \Cref{fig:6_buildschematic}(b) shows the fabricated samples. 
\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{6_buildschematic.jpg}
\caption{(a) Schematic of the build showing part geometry and corresponding energy density (MJ/sq.m) (b) Top view of the fabricated component}
\label{fig:6_buildschematic}
\end{figure}

\Cref{fig:6_EBSD_1} shows the top surface (melt pool) of the parts and corresponding grain morphology along the build direction (XZ plane). The results agree subjectively with the results explained in \Cref{fig:5_meltpool} and \Cref{fig:5_preheat}. Whenever distinct melt pools are visible on the top surface of the part, it results in columnar grains and whenever entire area is molten, it results in equiaxed grain structure. This essentially reiterates the numerical modeling results in \Cref{tab_ANOVA_ch5} which showed that preheat temperature of the substrate is crucial for equiaxed grain formation.   
\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{6_EBSD_1.jpg}
\caption{Top surface of the parts showing the melt pools and corresponding grain morphology along the build direction.}
\label{fig:6_EBSD_1}
\end{figure}
It is important to note that even though part 2 and part 5 have same spot scan parameters (pixel density and energy density), the resulting grain morphology is different. This is attributed to the change in geometry. Increasing the cross sectional area of the parts without increasing the energy density will result in different return time of the electron beam because of increase in number of pixels per layer. Using \Cref{fig:6_spot}, the return time is subjectively defined as the time difference between the beginning of melt 1 and beginning of melt 11. By increasing the cross sectional area by 4 times from $400 sq.mm$, to $1600 sq.mm$, the return time is increased by 4 fold. Because of this increase in return time in part 5, the energy deposited in the melt spot 1 is diffused through conduction and radiation. Melt spot 1 is already solidified before melt spot 11 hits (\Cref{fig:6_spot}). This results in lower local preheat temperature for melt spot 11. This initial condition with lower local preheat temperature applies to all the pixels in part 5. Absence of sufficient preheat temperature prevent any nucleation ahead of the solidification front thereby resulting in columnar morphology. Hence, increasing the energy density from part 5 to part 6, there is enough energy to preheat the substrate locally and maintain the area melt resulting in equiaxed grains. This shows that the columnar to equiaxed transition (CET) is highly geometry dependent and comprehensive tool path optimization is required to translate this result to a complex geometry.\\[8pt]
\subsection{Comparison of Texture}
To further quantify the grain structure, EBSD of the plane perpendicular (XY) to build direction was studied. \Cref{fig:6_XY} (a) and \Cref{fig:6_XY} (b) show the EBSD and texture of XY plane of part 5 and part 6 respectively. The EBSD data of XY plane reported in \Cref{fig:6_XY} can be compared with the EBSD of XY plane of sample fabricated through raster melt strategy reported in literature(\Cref{fig:2_sames} (a)). There is no surprise in the texture of part 5 as it is highly textured along the build direction. But for part 6, it is interesting to note that even though the grain morphology is equiaxed, it exhibits texture along the build direction. The texture has not been completely removed but weakened to an extent. Similar texture is observed in part 2, 3 and 4 with equiaxed morphology. It can be concluded that even though there is nucleation ahead of the solidification front, the majority of the direction of thermal gradient is still along the build direction. 
\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{6_XY.jpg}
\caption{EBSD and corresponding texture plot of (a) part 5 and (b) part 6 along the plane perpendicular (XY plane) to the build direction.}
\label{fig:6_XY}
\end{figure}  
\\[8pt]
\textbf{Demonstration build with hybrid microstructure}: Based on these results, a demonstration component (\Cref{fig:6_demo}) was fabricated using IN718 with hybrid grain morphology (columnar + equiaxed in same component). In the arms where equiaxed data is shown, a part with 20x20mm cross section was loaded separately and spot parameters corresponding to part 2 in \Cref{tab_ch6} was used. Rest of the part was built using normal raster scan strategy. 

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{6_demo.jpg}
\caption{A Demonstration component with hybrid microstructure }
\label{fig:6_demo}
\end{figure}   

\subsection{Analysis of Island Melt Strategy to Overcome Geometry Effect}
To decouple the effect of geometry and melt pattern, a new localized "island" or  "checkerboard" or "tile" melt scan strategy was designed. In this melt strategy, the layer to be melted was divided into smaller squares of \SI{5}{\milli\meter} each. Each square is termed as a "tile". For example, \Cref{fig:6_tile} shows a 20x20 mm geometry discretized into 16 5x5 mm tiles. Numbers within each tile dictates the sequence in which they will be melted. Skipping the tile is to maintain thermal isolation in such a way that melting of tile 2 doesn`t have significant thermal effect on the melting of tile 1 and so on. The hypothesis behind the tile melt sequence is that, if area melting can be induced within a tile (similar to \Cref{fig:6_EBSD_1}), equiaxed grain can be obtained in the area of the tile irrespective of the geometry.    
 
\begin{figure}
\centering
\captionsetup{justification=centering,margin=2cm}
\includegraphics[width=4in]{6_tile.jpg}
\caption{Schematic and sequence of tile melt strategy.}
\label{fig:6_tile}
\end{figure} 
  
Within each tile, spot melt strategy as shown in \Cref{fig:6_spot} was implemented. Number of pixels or spots within a tile can be varied along with beam current and dwell time. In addition, number of pixels to skip along X and Y direction (\Cref{fig:6_spot}) can also be varied. This affects the return time of the adjacent spot. A total of 16 samples were fabricated. \Cref{tab:tile_ch6} shows the variables used to fabricate the samples. 
\begin{table}[h!]
\centering
 \begin{tabular}{||C{1.75cm} | C{1.75cm} | C{1.75cm} | C{1.75cm} |  C{1.75cm} |  C{1.75cm} | C{1.75cm} ||} 
 \hline
 Part \# & Linear pixel count per tile & Pixel skip & Total pixels per tile & Beam Current (\SI{}{\milli\ampere}) & Beam ON Time (\SI{}{\milli\second}) & Energy per tile \linebreak (J)\\ [0.5ex] 
 \hline\hline
 1 & 10 & 2 & 100 & 10 & 1 & 60 \\ 
 
 2 & 10 & 4 & 100 & 10 & 1 & 60 \\ 
 
 3 & 10 & 6 & 100 & 10 & 1 & 60 \\ 
 
 4 & 10 & 8 & 100 & 10 & 1 & 60 \\ 
  
 5 & 10 & 2 & 100 & 5 & 2 & 60 \\ 
 
 6 & 10 & 4 & 100 & 5 & 2 & 60 \\
 
 7 & 10 & 6 & 100 & 5 & 2 & 60 \\ 
 
 8 & 10 & 8 & 100 & 5 & 2 & 60 \\  
 \hline
 9 & 12 & 2 & 144 & 10 & 1 & 86.4 \\ 
 
 10 & 12 & 4 & 144 & 10 & 1 & 86.4 \\ 
 
 11 & 12 & 6 & 144 & 10 & 1 & 86.4 \\ 
 
 12 & 12 & 8 & 144 & 10 & 1 & 86.4 \\ 
  
 13 & 12 & 2 & 144 & 5 & 2 & 86.4 \\ 
 
 14 & 12 & 4 & 144 & 5 & 2 & 86.4 \\
 
 15 & 12 & 6 & 144 & 5 & 2 & 86.4 \\ 
 
 16 & 12 & 8 & 144 & 5 & 2 & 86.4 \\
 \hline
\end{tabular}
\caption{Spot  melt parameter list to study the effect of geometry on CET}
\label{tab:tile_ch6}
\end{table}
\\[8pt]
Two sets of 8 samples were built. One with 100 pixels per tile and other with 144 pixels per tile essentially changing the amount of energy deposited per tile. Within one set of 8 samples, 4 were built with beam current of \SI{10}{\milli\ampere} and ON time of \SI{1}{\milli\second} and rest were built with beam current of \SI{5}{\milli\ampere} and ON time of \SI{2}{\milli\second}. Similar variation is performed for the other set of 8 samples with 144 pixels per tile. \Cref{fig:6_tilefab} shows the picture of as-built samples.  

\begin{figure}
\centering
\captionsetup{justification=centering,margin=2cm}
\includegraphics[width=5in]{6_tilefab.jpg}
\caption{Top view of fabricated samples.}
\label{fig:6_tilefab}
\end{figure}

The fabricated samples were then cut, mounted, polished and EBSD data was collected on the plane along the build direction (XZ plane). \Cref{fig:6_tileebsd} shows the sample number and corresponding EBSD data along the build direction. \Cref{fig:6_tileebsd}(a) corresponds to samples fabricated with beam current of \SI{10}{\milli\ampere} and ON time of \SI{1}{\milli\second} and \Cref{fig:6_tileebsd}(b) corresponds to samples fabricated with beam current of \SI{5}{\milli\ampere} and ON time of \SI{2}{\milli\second}. 

\begin{figure}
\centering
\captionsetup{justification=centering,margin=1cm}
\includegraphics[width=6in]{6_tileebsd.jpg}
\caption{EBSD of samples}
\label{fig:6_tileebsd}
\end{figure}

From, \Cref{fig:6_tilefab}, it can be seen that all the samples have complete area melt within all the tiles. According to previous conclusion in \Cref{fig:6_EBSD_1}, area melting should result in equiaxed grain structure. Earlier in \Cref{ch:chapter_5} (\Cref{tab_ANOVA_ch5}), it has been concluded that preheat temperature of the substrate is the most important factor in increasing the volume fraction of equiaxed grains within the melt pool. But, from \Cref{fig:6_tileebsd}, it can be observed that most of samples resulted in columnar grains even though the tile had area melt. But in samples 5 and 13, the columnar grains were broken with misoriented grains were observed. Only difference between sample 1 and 5 is the reduced current and increased ON time (\Cref{tab:tile_ch6}). The difference can be attributed to the competition between the energy input rate and the energy removal rate due to thermal diffusion and radiation. Even though same amount of energy is deposited per tile for sample 1 and 5, by increasing the beam ON time by twice and reducing the current by twice, the ratio of energy input rate per tile is reduced. In sample 1, it took \SI{100}{\milli\second} to deposit \SI{60}{\joule} of energy in one tile while in sample 2, it took \SI{200}{\milli\second} to deposit same amount of energy. There is more time for sample 2 to diffuse heat into the substrate compared to sample 1. Hence, it is fair to conclude that in sample 1, the energy deposition rate is high such that the substrate is not hot enough to reduce the thermal gradient at the liquid-solid interface and promote nucleation ahead of the solidification front. But in sample 2, the energy deposition rate is slow enough to promote some nucleation ahead of the interface. The results of samples 9 and 13 can be explained based on the similar argument. 
  
\section{Infrared Imaging}
Numerical modeling tools are computationally expensive to capture the variation in thermal signatures and corresponding solidification characteristics over an entire layer of the build. Hence, for efficient detection of solidification strucutre over larger time and length scales, a non-contact, in situ process monitoring technique has been employed. The method used in this section employs the algorithm and tool developed by Raplee et al. \cite{raplee2017thermographic}. 
\\[8pt]
Infrared thermal imaging is used to measure the solidification parameters and detect the microstructure variation in the build. In this experiment, a FLIR 7600 camera was focused onto the substrate inside the build chamber of an Arcam\textsuperscript{\textregistered} S12 through a circular window. The interaction of electron beam with the powder particles produces X-rays. To avoid damage due to X-rays, a lead glass is placed over the window. Since the electron beam energy source is highly dense, the liquid metal vaporizes and gets deposited on the circular window including the surface inside the chamber \cite{nandwana2016recyclability}. To reduce the metalization on the window, a rolling kapton film set up was used. The  frame rate was set at 100Hz. The physical set up of the system is comprehensively disccussed by Dinwiddie at al. \cite{dinwiddie2013thermographic}.
\\[8pt]
Infrared radiation is emitted by all the bodies except blackbody. IR camera has a sensor which records the intensity of the radiation. Intensity of the radiation depends on the (a) emissivity, (b) surface roughness or topology and (c) thermal energy of the emitting body. Higher the thermal energy higher the intensity of radiation recorded. In electron beam process, during melting, both preheated sintered powder and printed surface co-exist. Since the surface topologies of the sintered powder and printed surface are significantly different, each state of the system must be calibrated to convert the intensity to temperature of the respective regions. Calibration polynomials of the camera for as printed surface and sintered powder (IN718) was taken from Dinwiddie et al.\cite{dinwiddie2016calibrating}. 
\\[8pt]
\subsection{Algorithm for Calibration of IR data}
The raw infrared data is in the form of 3D matrix. At a given instant, the data is 2D and the third dimension is the time. At a given time, a given pixel in the raw IR data, can either be at sintered powder state or at printed solid state or at liquid metal state. Each of the states have their own emissivity. Hence each of the state has to be calibrated to convert the raw data to accurate temperature data. The temperature calibration was done by capturing the raw intensity data at different states and curve fitting the raw intensity to known temperature measure by thermocouple at solid and sintered powder state for the set up. Calibrating for liquid state is challenging and it has not been done. Method of calibration for IN718 and Ti-6Al-4V is comprehensively discussed by Dinwiddie et al. \cite{dinwiddie2016calibrating}. For IN718, the calibration equation for sintered powder and printed solid surface derived by Dinwiddie et al. \cite{dinwiddie2016calibrating} is given in \Cref{eqn:sinteredpowder_5} and \Cref{eqn:printedcalib_5} respectively. 

\begin{equation}
\label{eqn:sinteredpowder_5}
\begin{split}
T_{powder} \left(\textdegree C\right) &= -3.7979*10^{-22}I^6 + 4.2727*10^{-17}I^5 - 1.2895*10^{-12}I^4 \\ &+ 1.7210*10^{-8}I^3 - 1.1787*10^{-4}I^2 + 4.5893*10^{-1}I + 31.093 
\end{split}
\end{equation}

\begin{equation}
\label{eqn:printedcalib_5}
\begin{split}
T_{solid} \left(\textdegree C\right) &= 1.3905*10^{-20}I^6 + 5.8866*10^{-16}I^5 - 9.8350*10^{-12}I^4 \\ &+ 8.2644*10^{-8}I^3 - 3.7006*10^{-4}I^2 + 8.9133*10^{-1}I - 44.667 
\end{split}
\end{equation}
where $T_{powder}$ , $T_{solid}$ are the calibrated temprature of the sintered powder and printed solid respectively and $I$ is the raw intensity count recorded at each pixel.
\\[8pt]

During the process, a layer of powder is initially raked on the previous layer and is sintered using a preheat current. After that melting process begins. Depending on the 2D layer geometry, at the end of melting of a layer, only certain pixels are melted and converted into solid part and rest remains in the sintered powder state. Initially, the temporal data is calibrated at both the states (sintered powder and printed solid) for each pixel. But, the actual state of the pixel is unknown. The state of the pixel has to be identified to apply proper calibration equation. Before melting, the sintered powder equation has to be used and after melting as printed equation has to be used. To identify this transition region, Raplee et al. \cite{raplee2017thermographic} outlined an algorithm by studying the temporal behavior of each of the pixels. The details of the algorithm is given in \Cref{fig:6_thermoflowchart} and briefly discussed below. \\[8pt]

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=4in]{6_flowchart.jpg}
\caption{Flowchart describing the algorithm to identify the state of a pixel and calculate spatiotemporal G \& R data (recreated based on Raplee et al. \cite{raplee2017thermographic}.)}
\label{fig:6_thermoflowchart}
\end{figure}
       
When the melting occurs, there is an instantaneous drop in the calculated temperature at the pixel due to change in the emittance of the liquid state. To identify the correct frame at which the drop occurs, local slope is calculated and compared with a threshold value. The data is smoothed in advance to avoid noises. In addition, the unmelted pixels cool down due to radiation heat loss while the melting is happening. To distinguish the unmelted pixels, regional slope is calculated. The slope of melted pixel is significantly smaller than the unmelted pixel. \Cref{fig:6_IRslope} shows the difference in regional slope between the unmelted and melted pixel. 

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{6_IRslope.jpg}
\caption{Regional slope of (a) unmelted and (b) melted pixel. \cite{raplee2017thermographic}.}
\label{fig:6_IRslope}
\end{figure}

Once the transition point is identified and correct temperature calibration is applied before and after melting, the thermal gradient along X and Y axis and cooling rate are calculated in a similar manner explained in \Cref{subsec:GR_calc_ch3} (\Cref{G_eqn_ch3}, \Cref{Gx_eqn_ch3} and \Cref{GR_eqn_ch3}). From resultant thermal gradient and cooling rate, interface velocity can be calculated. At the end of solidification of a layer, spatial (2D surface) and temporal variation of G and R are captured. This can be plotted on a reference map and correlated to corresponding grain morphology. 
  
\subsection{Experimental Observation}
For a fixed geometry, the melt strategy and process parameters have already been identified \Cref{tab_ch6} to obtain the required solidification microstructure. For this study, 2 samples (20x20x20 mm) were fabricated. For sample 1, the linear pixel density was set at 4 and for sample 2, the linear pixel density was set at 6. From \Cref{tab_ch6} and \Cref{fig:6_EBSD_1}, it can be observed that sample 1 will result in columnar grain and sample 2 will result in equiaxed grain.

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{6_GR.jpg}
\caption{G and R calculated from IR data for (a) sample 1 (b) sample 2.}
\label{fig:6_GR}
\end{figure} 

\Cref{fig:6_GR} shows the heat map of temperature gradient and liquid-solid interface velocity calculated from the IR data. The square region in \Cref{fig:6_GR} (a) corresponds to the surface of the sample 1 and the peripheral noises are due to sintered powder surrounding the sample. Data for sample 2 (\Cref{fig:6_GR} (b)) is interesting to observe. Significant amount of noise can be observed. Since surface topology can affect the intensity, to rationalize this, the surface topology of the samples were analyzed. From \Cref{fig:6_keyence}, it can be observed that surface of sample 1 is almost flat and the variation is approximately \SI{50}{\micro\meter}. For sample 2, the surface is skewed (convex) by about \SI{500}{\micro\meter}. Correlation between the keyence data (\Cref{fig:6_keyence}(b)) and the thermal data (\Cref{fig:6_GR} (b)) can be observed. the effect of concavity and convexity on the collected intensity is discussed by Raplee et al. \cite{raplee2017thermographic}.

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{6_keyence.jpg}
\caption{Surface topology of (a) sample 1 and (b) sample 2.}
\label{fig:6_keyence}
\end{figure}   

Another important thing to note in the thermal data of sample 2 (\Cref{fig:6_GR} (b)) is that, the top most region has high thermal gradient and low velocity corresponding to the columnar regime. Keyence data of the same region is not skewed and rightly so, the effect of topology can be ruled out. It can be correlated to the spot melting sequence described in \Cref{fig:6_spot}. It should be noted that the bands of spots moves from top left to the bottom right, hence the top  most region will not have interaction with the spots in upcoming sequences once the top few bands of pixels are melted. G and R are plotted on the reference solidification map. \Cref{fig:6_thermoGR} (a) and (b) show the distribution of G and R for sample 1 and sample 2 respectively. It can be observed that the data for sample 1 lies completely in the columnar regime but sample 2 is widely distributed across. As mentioned earlier, the variation can be attributed to the noise due to non-flat surface topology but can also be attributed to the top most region where the gradient is higher.  

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{6_thermoGR.jpg}
\caption{G and R overlay on reference solidification map \cite{nastac2001advances} of IN718 for (a) sample 1 and (b) sample 2.}
\label{fig:6_thermoGR}
\end{figure}

To validate it experimentally, EBSD was done at top and bottom region of the sample 2 as pointed out in  \Cref{fig:6_edge}. \Cref{fig:6_edge} (a) and (b) shows the EBSD and grain aspect ratio of the edge (XZ plane) where the thermal gradient is high and low respectively. It can be observed that, at one edge of the sample the grains grow in columnar orientation and at the other edge the grains are equiaxed. This result correlates with the data shown in \Cref{fig:6_GR} (b) and \Cref{fig:6_thermoGR} (b).      

\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{6_edge.jpg}
\caption{EBSD and aspect ratio map of (a) top region and (b) bottom region of sample 2.}
\label{fig:6_edge}
\end{figure}  

\section{Summary}
Three topics have been discussed in this chapter. 
\begin{itemize}
  \item Effect of build geometry on columnar to equiaxed transition has been analyzed. It has been concluded that same melt strategy and process parameters that resulted in equiaxed grains for a geometry can not be used for a different geometry to induce equiaxed grains. Appropriate process parameter modifications have to be made as a function of geometry. 
  \item A demonstration component was built with both columnar and equiaxed grains within the same component. 
  \item A localized "island" melt scan strategy has been evaluated. The importance of the competition between energy deposition rate and diffusion rate has been discussed. Further modeling and experimental work is necessary to understand and analyze the thermal behavior of the proposed scan strategy.
  \item Usage of in-situ process monitoring technique of infrared imaging to detect the grain morphology has been discussed based on a published algorithm. Correlation of surface topology and noise in the IR data has been reported.
\end{itemize}