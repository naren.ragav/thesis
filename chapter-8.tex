\chapter{Conclusion and Path Forward}
\label{ch:chapter_8}
\section{Summary}

\begin{itemize}
\item Significance of tool path design to control the solidification microstructure in metal additive manufacturing process has been reported.
\item A parallel numerical modeling tool has been used to predict the spatiotemporal variation of the solidification parameters (a) Thermal gradient at the liquid-solid interface \textbf{(G)} (b) Velocity of the liquid-solid interface \textbf{(R)}. 
\item A new scan strategy has been introduced for on-demand control of solidification grain size and primary dendrite arm spacing. Numerical tool was used to rationalize the effect of the scan strategy on the grain size by quantifying the shape of of the molten pool. Increase in curvature of the melt pool resulted in finer grains. In addition to epitaxial solidification, possibility of heterogeneous nucleation has been analyzed based on the variation in cooling rate predicted using the simulations. Conventional raster pattern resulted in columnar grain size of about \SI{700}{\micro\meter}, while using the modified scan strategy, the grain size was reduced to about \SI{30}{\micro\meter}. 
\item Predicted spatio-temporal G and R data was coupled with theoretical model to quantify the volume fraction of equiaxed grains formed within a melt pool during solidification. Relative significance of different beam input parameters on equiaxed grain formation was statistically analyzed through design of experiments approach. Preheat temperature was found to be the most significant factor in contributing to the increase in stray grain formation. Appropriate scan strategy was designed and bulk columnar to equiaxed transition has been experimentally verified.
\item In addition to the effect of scan strategy, geometry was also found to play a crucial role in columnar to equiaxed grain morphology transition. The parameters of the scan strategy has to be changed appropriately with respect to theh geometry to obtain the desired microstructure.  A localized "tile" melt strategy was introduced to overcome the effect of geometry and the competition between the energy deposition rate and energy diffusion rate has been analyzed. 
\item In-situ process monitoring tool (IR imaging) was employed to capture the spatial variation of the solidification parameters (G and R). Spatially varying G,R was correlated to spatially varying microstructure (grain morphology).
\item Effect of surface tension on melt pool shape and volume fraction of stray grain formation was studied. Flow patterns for low and high surfactant concentration has been compared. Competition between convective heat transfer rate and conductive heat transfer rate was analyzed by calculating the Peclet number within the melt pool. 
\item In addition, the importance of fluid flow with respect to the scan strategy (energy density) has been discussed by comparing the simulation data with the experimental data (width and depth of melt pool). Fluid flow was found to play a key role in altering the shape of the melt pool when the energy deposition density is higher and vice versa.   
\end{itemize}

\section{Path Forward}
There are numerous pathways to improvise the reported modeling and experimental framework. 
\\[8pt]
\textbf{Other AM processes and Materials:} Is it possible to translate the results of this work across different AM processes and material systems? What is the significance of scan strategy in laser powder bed system, laser powder blown system and wire based AM systems? Few other interesting improvisations and questions to be addressed are listed below.
\\[8pt]
\textbf{Complex Geometry:} So far in this work, effect of scan strategy in complex geometry has not been studied. \Cref{fig:8_blade} shows the different scan strategy implemented on an airfoil. The airfoil layer is discretized into 115 pixels or spots. Color scale corresponds to the time stamp or the sequence of each spot with respect to the scan pattern. \Cref{fig:8_blade} (a) is the normal raster pattern and \Cref{fig:8_blade} (b) is the "Dehoff" fill which is same as the spot fill pattern mentioned in \Cref{fig:6_spot} but for complex geometry. 

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{8_blade.jpg}
\caption{Sequence of fill in an airfoil (a) Raster fill (b) Dehoff fill or spot fill.}
\label{fig:8_blade}
\end{figure}

Corresponding G and R plot is given in \Cref{fig:8_GRblade}. There is no significant difference between the 2 cases. Effect of scan pattern on complex geometry was discussed briefly in this section and further investigation is necessary. Innovative scan strategies have to be taken into consideration to achieve on-demand solidification microstructure control for complex geometries. This is just a proof of concept that the framework and proposed approach can be used for complex geometries. Detailed investigation can be made to design the scan strategy for complex geometry. 

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=5in]{8_GRblade.jpg}
\caption{G vs R for the raster fill and "Dehoff" fill}
\label{fig:8_GRblade}
\end{figure}

\textbf{Fidelity Analysis and Uncertainty Quantification:} Through the knowledge gained from this work, it would be interesting to analyze the effect of the fidelity of the numerical models on solidification microstructure. Is it vital to include the beam-powder interaction in the model? Is free surface modeling important to understand the solidification microstructure? Models across scales have to be coupled with uncertainty quantification tools like Dakota \cite{adams2009dakota} and PUQ \cite{hunt2015puq} to check for the sensitivity of the output as a function of input material properties. 
\\[8pt]
\textbf{Alloy Design for AM:} Most of the current researches in AM are focussed on processing existing commercial alloys which are processed through casting. Since AM is highly transient in nature, the solidification phenomenon including segregation, undercooling and solid state precipitation will be different compared to casting processes. Alloy design/modifications in specific for AM is an important topic to be discussed. Is it possible to modify alloy compositions and/or add innoculants/nucleating agents which can promote equiaxed grains for complex geometries but at the same time maintain the columnar growth when necessary within the processing window? Recent work by Martin et al. \cite{martin20173d} showed the possibility of mitigating cracking of additively fabricated high strength Aluminum alloys by adding innoculants in the form of Zr nanoparticles and producing equiaxed grains. However the thermodynamic reasoning behind the choice of nucleant was not discussed comprehensively. Modification of alloy chemistry based on interface response function theory for columnar to equiaxed transition is an important research topic to be addressed.     
\\[8pt]
\textbf{Integration With Machine Learning Algorithms:} One of the significant unanswered questions in this work of controlling grain morphology is the design of scan strategy for complex geometries. As described in \Cref{ch:chapter_5}(\Cref{tab_ANOVA_ch5}), for any given geometry, the grain morphology control is dictated by the local preheat temperature of the substrate. Local preheat temperature is dictated  by the competition between the energy deposition rate and energy dissipation rate.  Energy deposition rate is controlled by the frequency of the adjacent spots in the scan strategy. Frequency analysis has to be done to control the energy deposition rate. Since complex geometries will have complex boundary conditions (overhangs vs no-overhangs - powder vs solid), the energy dissipation rate will vary with respect to the geometry. Energy dissipation rate can be studied as a function of geometry by coupling the numerical modeling with machine learning algorithms. But simulating the entire part will be computationally expensive. A brief suggestion that can be developed into a comprehensive research work is detailed below. In any geometry, highly transient heat transfer effect can be observed only in the top few layers \Cref{fig:8_matrix} (a). The rest of the previously built layers will be at steady state (comparatively). Hence, it is sufficient to understand the effect of processing parameters in the transient region to control the solidification microstructure. The transient region can then be spatially discretized into a binary matrix format of 1`s and 0`s. 1`s represent solid (bulk) part and 0`s represent powder region. This essentially represent a complex geometry with complex boundary conditions. \Cref{fig:8_matrix} (b) shows the matrix format of a triangle built without any overhangs and \Cref{fig:8_matrix} (c) shows the triangle with overhangs. Numerical simulations can be performed for different frequencies of the spots and different boundary conditions. The results can be analyzed by using sophisticated machine learning algorithms and a surrogate model can be created. The surrogate model can be used to reduce the computational expense and optimize the scan strategy as a function of geometry.   

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.25cm}
\includegraphics[width=6in]{8_matrix.jpg}
\caption{(a) Variation in transient nature as a function of build height (b) Matrix format of complex geometry (triangle) without overhang (c) with overhang. }
\label{fig:8_matrix}
\end{figure}
\textbf{Integrated Computational Framework:} Looking from a larger perspective, the numerical codes across different time and length scales can be coupled along with machine learning algorithms to develop an integrated computational framework to predict the process-structure-property linkage. Schematic of the proposed approach is given in \Cref{fig:8_ICME}. Using this framework, the lead time and experimental expense in designing new alloy systems and time taken to optimize the process parameters can be reduced significantly. 
\begin{figure}
\centering
\captionsetup{justification=centering,margin=0.25cm}
\includegraphics[width=6in]{8_ICME.jpg}
\caption{Integrated Computational and Machine Learning Framework to predict process-structure-property in AM process.}
\label{fig:8_ICME}
\end{figure}
  
