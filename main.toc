\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Introduction to Additive Manufacturing Process}{1}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Business and Environmental Implications of Additive Manufacturing Technology}{2}{subsection.1.1.1}
\contentsline {subsubsection}{Industrialization of Metal AM}{5}{section*.17}
\contentsline {subsection}{\numberline {1.1.2}A Brief Introduction to Arcam\textsuperscript {\textregistered } process}{5}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Alloys used in powder bed AM processes}{7}{subsection.1.1.3}
\contentsline {section}{\numberline {1.2}Factors Affecting Mechanical Performance of Engineering Components}{8}{section.1.2}
\contentsline {chapter}{\numberline {2}Literature Review and Scientific Gaps}{10}{chapter.2}
\contentsline {section}{\numberline {2.1}Literature Review}{10}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Fundamentals of Solidification}{10}{subsection.2.1.1}
\contentsline {subsubsection}{Nucleation}{10}{section*.21}
\contentsline {subsubsection}{Growth}{15}{section*.25}
\contentsline {subsection}{\numberline {2.1.2}Evolution of Microstructure in Rapid Solidification Processes}{19}{subsection.2.1.2}
\contentsline {subsubsection}{Lessons from Welding Literature}{19}{section*.30}
\contentsline {subsection}{\numberline {2.1.3}Electron Beam Welding}{21}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Modeling of Rapid Solidification Process}{24}{subsection.2.1.4}
\contentsline {subsubsection}{Analytical Models}{24}{section*.35}
\contentsline {subsubsection}{Numerical Models}{26}{section*.37}
\contentsline {subsubsection}{Feasibility of Site-Specific Microstructure Control in AM}{28}{section*.40}
\contentsline {section}{\numberline {2.2}Scientific Gaps and Research Plan}{32}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Research Gaps}{32}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Material System}{32}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Numerical Code}{33}{subsection.2.2.3}
\contentsline {subsubsection}{Why Truchas?}{34}{section*.44}
\contentsline {subsection}{\numberline {2.2.4}Experimental Methodology}{35}{subsection.2.2.4}
\contentsline {subsubsection}{Part Fabrication}{35}{section*.45}
\contentsline {subsubsection}{Characterization}{35}{section*.46}
\contentsline {chapter}{\numberline {3}Overview of Numerical Modeling Framework}{36}{chapter.3}
\contentsline {section}{\numberline {3.1}Truchas}{36}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Conservation Equations}{36}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Heat Source Profile}{37}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Limitations and Assumptions of the model}{39}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Initial and Boundary Conditions}{39}{subsection.3.1.4}
\contentsline {subsection}{\numberline {3.1.5}Geometry and Meshing}{40}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Scaling of the Code}{40}{subsection.3.1.6}
\contentsline {subsection}{\numberline {3.1.7}Material Properties}{42}{subsection.3.1.7}
\contentsline {subsection}{\numberline {3.1.8}Calculation of G and R}{42}{subsection.3.1.8}
\contentsline {chapter}{\numberline {4}Grain size and Primary Dendrite Arm Spacing control}{47}{chapter.4}
\contentsline {section}{\numberline {4.1}Influence of Grain Size on Mechanical Properties}{47}{section.4.1}
\contentsline {section}{\numberline {4.2}Melt-Scan Strategy}{48}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Conventional Melt Sequence}{48}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Proposed Melt Strategy}{49}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Experimental Set up}{50}{section.4.3}
\contentsline {section}{\numberline {4.4}Results and Discussions}{51}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Melt pool shape}{51}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Variation in Primary Dendrite Arm Spacing}{54}{subsection.4.4.2}
\contentsline {subsubsection}{Theory}{54}{section*.56}
\contentsline {subsubsection}{Experimental Observation}{55}{section*.57}
\contentsline {subsubsection}{Numerical Prediction}{58}{section*.61}
\contentsline {subsection}{\numberline {4.4.3}Variation in Grain Size}{61}{subsection.4.4.3}
\contentsline {subsubsection}{Theory}{61}{section*.65}
\contentsline {subsubsection}{Experimental Observation}{63}{section*.66}
\contentsline {subsubsection}{Epitaxial Growth}{64}{section*.69}
\contentsline {subsubsection}{Heterogeneous Nucleation and Growth}{67}{section*.71}
\contentsline {section}{\numberline {4.5}Summary}{71}{section.4.5}
\contentsline {chapter}{\numberline {5}Evaluation of Columnar to Equiaxed Transition (CET)}{72}{chapter.5}
\contentsline {section}{\numberline {5.1}Motivation}{72}{section.5.1}
\contentsline {section}{\numberline {5.2}Qualitative Analysis of Parameters}{73}{section.5.2}
\contentsline {section}{\numberline {5.3}Quantitative Analysis}{76}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}Theory}{76}{subsection.5.3.1}
\contentsline {subsection}{\numberline {5.3.2}Calculation of Spatio-temporal $\Phi $}{77}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}ANOVA}{78}{subsection.5.3.3}
\contentsline {section}{\numberline {5.4}Experimental Validation}{81}{section.5.4}
\contentsline {subsection}{\numberline {5.4.1}Scan Strategy and Fabrication}{81}{subsection.5.4.1}
\contentsline {subsection}{\numberline {5.4.2}Insignificance of Beam Diameter}{81}{subsection.5.4.2}
\contentsline {subsection}{\numberline {5.4.3}Significance of Preheat Temperature}{83}{subsection.5.4.3}
\contentsline {section}{\numberline {5.5}Summary}{85}{section.5.5}
\contentsline {chapter}{\numberline {6}Geometry Dependency of CET and Infrared In-situ Monitoring}{88}{chapter.6}
\contentsline {section}{\numberline {6.1}Effect of Build Geometry on CET}{88}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Scan Strategy and Parameters}{89}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Comparison of Texture}{91}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Analysis of Island Melt Strategy to Overcome Geometry Effect}{94}{subsection.6.1.3}
\contentsline {section}{\numberline {6.2}Infrared Imaging}{99}{section.6.2}
\contentsline {subsection}{\numberline {6.2.1}Algorithm for Calibration of IR data}{100}{subsection.6.2.1}
\contentsline {subsection}{\numberline {6.2.2}Experimental Observation}{101}{subsection.6.2.2}
\contentsline {section}{\numberline {6.3}Summary}{105}{section.6.3}
\contentsline {chapter}{\numberline {7}Effect of Fluid Flow and Surface Tension Gradient on Solidification}{108}{chapter.7}
\contentsline {section}{\numberline {7.1}Effect of Surface Tension on Convection}{108}{section.7.1}
\contentsline {section}{\numberline {7.2}Conservation Equations}{109}{section.7.2}
\contentsline {section}{\numberline {7.3}Simulation and Results}{110}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}Preaheat Temperature: 25\textdegree C}{112}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}Preaheat Temperature: 1000\textdegree C}{113}{subsection.7.3.2}
\contentsline {subsection}{\numberline {7.3.3}Comparison of Peclet number}{114}{subsection.7.3.3}
\contentsline {subsection}{\numberline {7.3.4}Effect of fluid flow on microstructure}{116}{subsection.7.3.4}
\contentsline {subsection}{\numberline {7.3.5}Significance of Scan Strategy on Fluid Flow and Experimental Comparison of melt pool shape:}{117}{subsection.7.3.5}
\contentsline {section}{\numberline {7.4}Summary}{120}{section.7.4}
\contentsline {chapter}{\numberline {8}Conclusion and Path Forward}{122}{chapter.8}
\contentsline {section}{\numberline {8.1}Summary}{122}{section.8.1}
\contentsline {section}{\numberline {8.2}Path Forward}{123}{section.8.2}
\contentsline {chapter}{Bibliography}{128}{section*.112}
\contentsline {chapter}{Appendices}{142}{section*.113}
\contentsline {chapter}{\numberline {A}Appendix-1}{143}{appendix.A}
\contentsline {section}{\numberline {A.1}Nucleation Rate as a Function of Cooling Rate}{143}{section.A.1}
\contentsline {chapter}{\numberline {B}Appendix-2}{145}{appendix.B}
\contentsline {section}{\numberline {B.1}CUBIT: Mesh Generation Script Template}{145}{section.B.1}
\contentsline {section}{\numberline {B.2}Python script to filter G and R at interface}{146}{section.B.2}
\contentsline {chapter}{Vita}{150}{section*.114}
