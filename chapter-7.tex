\chapter{Effect of Fluid Flow and Surface Tension Gradient on Solidification}
\label{ch:chapter_7}
The numerical modeling results in \Cref{ch:chapter_4} and \Cref{ch:chapter_5} considers only the physics of heat transfer and phase change. Effect of convection within the melt pool is discussed in this chapter. 
\section{Effect of Surface Tension on Convection}
Oreper et al. \cite{oreper1983convection} and Kou et al. \cite{kou1986weld} pioneered the development of 2D and 3D numerical tools repectively to study the convection within the melt pool during welding. Effect of convection within the molten pool has been extensively studied using numerical modeling by Zacharia et al. \cite{zacharia1989three} and Debroy et al. \cite{zhang2003modeling, rai2008heat, he2003heat}. Depending on the forces acting on the liquid within the molten pool, the convection might have significant effect on the solidification microstructure and mechanical properties. Heiple \cite{heiple1981effect, heiple1982mechanism, heiple1981effects, heiple1983surface, heiple1985effects} reported various experimental investigations on variation in shape of the fusion zone during welding of stainless steel and other alloys. The variation was correlated to variation in elemental fraction cetain elements and impurities in the base metal. During electron beam welding, 4 major forces affect the fluid flow including buoyancy, electromagnetic, drag and surface tension or thermocapillary force. In general, among all the forces, surface tension force is found to be the most dominant during electron beam welding \cite{heiple1982mechanism}. Surface tension is a function of temperature. For pure metals, the variation of surface tension with respect to temperature $\left(d\gamma/{dT}\right)$ is negative. But addition of certain surface active elements over a threshold level is found to alter the sign of surface tension gradient with respect to temperature from negative to positive. For example, in Fe and Ni alloys, when concentration of Sulfur and Oxygen is increased above 10 ppm, the value $\left(d\gamma/{dT}\right)$ becomes positive at 1600\textdegree C \cite{sahoo1988surface, }. The resultant flow due to surface tension gradient is termed as Thermocapillary or Marangoni convection. Sahoo et al. \cite{sahoo1988surface} reported theoretical analysis of the effect of surface active elements (Oxygen, Sulfur, Selenium and Tellurium) on surface tension gradient. These elements alter the surface tension gradient thereby altering the direction of the flow within the melt pool. Theoretical relationship derived by Sahoo et al. \cite{sahoo1988surface, mills1990factors} for calculation of surface tension as a function of temperature and solute is given in \Cref{sahoo_eqn_ch7}.
\begin{equation}
\label{sahoo_eqn_ch7}
\gamma = {\gamma}^o - A\left(T-T_m\right) - RT\Gamma_s ln\left(1+ka*exp\left(-\Delta H_o/RT\right)\right)
\end{equation}
where $\gamma$ is the surface tension of metal plus solute, $\gamma^o$ is the surface tension of pure metal, $T$ is the temperature, $T_m$ is the melting point, $R$ is gas constant, $\Gamma_s$ is surface excess at saturation, $k$ is a constant related to entropy of segregation and $\Delta H_o$ heat of adsorption.

The direction of fluid flow is always from low tension to high tension region. \Cref{fig:7_schematic} shows the the variation of flow pattern with respect to change in surface gradient with respect to temperature. Negative surface tension gradient 
\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=4in]{7_schematic.jpg}
\caption{(a) Variation of surface tension with temperature (b) Top surface flow (c) Side view of the flow pattern}
\label{fig:7_schematic}
\end{figure}

\section{Conservation Equations}
In addition to diffusive energy transfer, convection also plays vital role within the melt pool. Hence, to increase the accuracy of the prediction, the fluid dynamics in the molten pool has to be accounted for in the model. To account for fluid dynamics, an advection term has to be added to the energy conservation equation (\Cref{conserv_eqn_ch3}). In addition to solving conservation of energy, mass and momentum equations have to solved simultaneously. The 3 conservation equations are given by \Cref{energy_eqn_ch7}, \Cref{mass_eqn_ch7} and \Cref{moment_eqn_ch7} respectively.

\begin{equation}
\label{energy_eqn_ch7}
\frac{\partial {\left(\rho h\right)}}{\partial t} + \bigtriangledown . \left(\rho^Lh^Lf^Lu\right)- \bigtriangledown . \left(k\bigtriangledown T\left(h\right)\right) =  \dot{S}
\end{equation} 
where $f^L$,$h^L$ and $u$ are volume fraction of liquid in the node and specific enthalpy of the liquid and velocity respectively.
\begin{equation}
\label{mass_eqn_ch7}
\frac{\partial {\rho}}{\partial t} + \bigtriangledown . \left(\rho u\right) =  0
\end{equation} 
\begin{equation}
\label{moment_eqn_ch7}
\frac{\partial {\rho u}}{\partial t} + \bigtriangledown . \left(\rho{uu}\right) =  -\bigtriangledown p + \bigtriangledown.\tilde{\tau} + S
\end{equation}
where $\rho$ is density, $p$ is pressure, $\tilde{\tau}$ is the shear stress tensor and $S$ is the source term. Source term can contain body forces, surface tension forces, drag force etc. 
\\[8pt]
\section{Simulation and Results}
A single spot is simulated with beam current of 20 mA and on time of 1 ms. Since in \Cref{ch:chapter_5}, it was figured out that preheat temperature plays crucial role in stray grain formation, in this section two extreme cases of initial conditions were considered (25\textdegree C and 1000\textdegree C). In addition, effect of different levels of surfactant concentration on the fluid flow pattern has been studied. A total of 4 simulations with different combinations of preheat temperature and surfactant concentration have been done. Thermophysical properties used in the simulations are given in \Cref{prop_tab_ch3}. Surface tension gradient for low (6 ppm sulfur, < 10 ppm Oxygen) and high (20 ppm sulfur, 8 ppm Oxygen) surfactant concentration used in the simulations is given in \Cref{fig:7_surfacetension}. 
\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=4.5in]{7_surfacetension.jpg}
\caption{Surface Tension as a function of temperature for IN718 with different surfactant concentration \cite{lee1998modelling}}
\label{fig:7_surfacetension}
\end{figure}
Surface tension $\left(Nm^{-1}\right)$ and surface tension gradient $\left(Nm^{-1}K^{-1}\right)$ as a function of temperature for low surfactant concentration are given by \Cref{STLS_eqn_ch7} and \Cref{STGLS_eqn_ch7} respectively. The transition temperature where the surface tension gradient flips the direction is \SI{1740}{\kelvin}.
\begin{equation}
\label{STLS_eqn_ch7}
\gamma = \left(1.02308E-10\right)T^3 - \left(0.84788E-6\right)T^2 + \left(2.02088E-3\right)T + 0.236238505
\end{equation}
\begin{equation}
\label{STGLS_eqn_ch7}
\frac{d\gamma}{dT} = \left(13.06924E-10\right)T^2 - \left(1.6958E-6\right)T + 2.02088E-3
\end{equation}

Surface tension $\left(Nm^{-1}\right)$ and surface tension gradient $\left(Nm^{-1}K^{-1}\right)$ as a function of temperature for high surfactant concentration are given by \Cref{STHS_eqn_ch7} and \Cref{STGHS_eqn_ch7} respectively. The transition temperature where the surface tension gradient flips the direction is \SI{1895}{\kelvin}.
\begin{equation}
\label{STHS_eqn_ch7}
\gamma = \left(1.4849E-10\right)T^3 - \left(1.2436E-6\right)T^2 + \left(3.1122E-3\right)T + 0.7539
\end{equation}
\begin{equation}
\label{STGHS_eqn_ch7}
\frac{d\gamma}{dT} = \left(4.45455E-10\right)T^2 - \left(2.48721E-6\right)T + 3.1122E-3
\end{equation}

\subsection{Preaheat Temperature: 25\textdegree C}
Since the melt pool is a single spot, four fold symmetry is applied and quarter of the melt pool has been simulated for all the cases. As described in \Cref{subsec:GR_calc_ch3}, the G and R data is calculated for these simulations. \Cref{fig:7_25C_G} compares the spatially varying $G$ data along the depth of the melt pool (XZ plane) for pure heat transfer simulation and fluid flow simulations with low and high surfactant concentration for preheat of 25\textdegree C. \Cref{fig:7_25C_G} also provides comparison of melt pool shapes for different cases. From \Cref{fig:7_25C_G}, it can be concluded that there is no significant difference in the sub-surface shape of the melt pool.  

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_25C_G.jpg}
\caption{Thermal gradient $G$ (a) Pure Heat transfer (b) Fluid flow (Low Surfactant) (c) Fluid flow (High Surfactant)}
\label{fig:7_25C_G}
\end{figure}

\Cref{fig:7_25C_Velocity} shows the comparison of fluid flow pattern at different time of the simulation. Images in top row corresponds to low surfactant concentration and images in bottom row corresponds to high surfactant concentration. The heat source was turned ON for 1 ms. From \Cref{fig:7_25C_Velocity}(a) at \SI{3}{\milli\second} (3ms after the the beam is turned on), it can be seen that for both the cases, the direction of flow is in the same direction (outwards flow). At \SI{3.5}{\milli\second}, low surfactant concentration maintains the direction of flow but the high surfactant concentration starts to flip the direction of the flow at te edge of the melt pool with both inward and outward flow co-existing. At \SI{4.5}{\milli\second}, the direction of flow for high surfactant concentration case is completely inwards while the low surfactant concentration just begins the flip the direction with both flows co-existing. 
\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_25C_Velocity.jpg}
\caption{Comparison of fluid flow pattern for low and high surfactant concentration at (a) \SI{3}{\milli\second} (b) \SI{3.5}{\milli\second} (c) \SI{4.5}{\milli\second}}
\label{fig:7_25C_Velocity}
\end{figure}

\subsection{Preaheat Temperature: 1000\textdegree C}
\Cref{fig:7_1000C_G} compares the spatially varying $G$ data along the depth of the melt pool (XZ plane) for pure heat transfer simulation and fluid flows simulations with low and high surfactant concentration for preheat of 1000\textdegree C. \Cref{fig:7_1000C_G} also provides comparison of melt pool shapes for different cases. Unlike the simulations at low preheat, from \Cref{fig:7_1000C_G}, difference in the sub-surface shape of the melt pool can be observed due to 'lipping' effect near the top edge of the melt pool. This can be attributed to the flow pattern within the molten pool.  

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_1000C_G.jpg}
\caption{Thermal gradient $G$ (a) Pure Heat transfer (b) Fluid flow (Low Surfactant) (c) Fluid flow (High Surfactant)}
\label{fig:7_1000C_G}
\end{figure}

\Cref{fig:7_1000C_Velocity} shows the comparison of fluid flow pattern at different time of the simulation. Images in top row corresponds to low surfactant concentration and images in bottom row corresponds to high surfactant concentration. In this case also, the beam was turned ON for 1 ms. From \Cref{fig:7_1000C_Velocity}(a) at \SI{4}{\milli\second}, it can be seen that for low surfactant cases, the direction of flow is outward and for the high surfactant case, the majority of the flow is in outwards direction but, at the edge of the molten pool, the direction begins to flip. At \SI{5}{\milli\second}, low surfactant concentration maintains the direction of flow and start to flip the direction at the edge but the direction of flow for high surfactant concentration is completely reversed (inward). At \SI{8}{\milli\second}, for both the cases, the direction of flow is inward but the maximum velocity of flow for high surfactant case is found to be higher at $0.3 m/s$ compared to $0.13 m/s$ for low surfactant case. Non-dimensional analysis of the modes of heat transfer is reported in the next section. 
\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_1000C_Velocity.jpg}
\caption{Comparison of fluid flow pattern for low and high surfactant concentration at (a) \SI{4}{\milli\second} (b) \SI{5}{\milli\second} (c) \SI{8}{\milli\second}}
\label{fig:7_1000C_Velocity}
\end{figure}
%\Cref{fig:7_GRcomp} shows the comparison of distribution of G and R data plotted on a reference solidification map. It can be observed that there is significant amount of scatter in the data for simulations with fluid flow included. 
%\begin{figure}[!]
%\centering
%\captionsetup{justification=centering,margin=0.75cm}
%\includegraphics[width=4in]{7_GRcomp.jpg}
%\caption{Comparison of G and R for pure heat transfer and heat transfer plus fluid flow simulations for the preheat of 1000\textdegree C and at room temperature 25\textdegree C. }
%\label{fig:7_GRcomp}
%\end{figure}
\subsection{Comparison of Peclet number}
Peclet number is a dimensionless number and is defined as the ratio of convective heat transfer to the diffusive heat transfer. In a melt pool, calculation of Peclet number describes the dominating heat transfer mechanism during melting and solidification. Peclet number is given by \Cref{eqn:peclet_ch7}.  

\begin{equation}
\label{eqn:peclet_ch7}
Pe = \frac{Convective Heat Transfer Rate}{Diffusive Heat Transfer Rate}
Pe = \frac{L.U}{\alpha}
\end{equation}

where $U$ is the velocity, $L$ is the characteristic length and $\alpha$ is the thermal diffusivity of the material. In this case spatially varying grid Peclet number is calculated by using the velocity data at each of the discretized elements in the melt pool and $L$ is taken as the size of the element ($\delta x$) \cite{leonard1979stable}. Thermal diffusivity is calculated from the specific heat capacity and thermal conductivity values listed in \Cref{prop_tab_ch3}. \Cref{fig:7_Peclet_100} and \Cref{fig:7_Peclet_1000} shows the Peclet number calculated for 25\textdegree C and 1000\textdegree C respectively with the maximum Peclet number mentioned for each data. 

\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_Peclet_100.jpg}
\caption{Comparison of Peclet number for low and high surfactant concentration with preheat of 25\textdegree C at (a) \SI{3}{\milli\second} (b) \SI{3.5}{\milli\second} (c) \SI{4.5}{\milli\second}}
\label{fig:7_Peclet_100}
\end{figure}
  
\begin{figure}[h]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_Peclet_1000.jpg}
\caption{Comparison of Peclet for low and high surfactant concentration with preheat of 1000\textdegree C at (a) \SI{4}{\milli\second} (b) \SI{5}{\milli\second} (c) \SI{8}{\milli\second}}
\label{fig:7_Peclet_1000}
\end{figure}

If the $Pe$ is equal to 1, both convection and conduction play equal role. In general, $Pe$ is high at the surface of the melt pool. In both the cases, the conductive heat transfer begin to dominate as the time proceeds. But, convective heat transfer is much more prominent for the alloy with high surfactant concentration compared to low surfactant concentration. For example, in \Cref{fig:7_Peclet_100} (c), for high S content, the maximum $Pe$ is 0.56 compared to 0.16 for low S alloy. This difference in $Pe$ between different S concentration might alter the volume fraction of stray grains formed in melt pool. 

\subsection{Effect of fluid flow on microstructure}
In order to check the influence of fluid flow and surfactant concentration on the solidification microstructure, the predicted $G$ and $R$ data is then compared with pure heat conduction simulation for the same parameters. Weighted average of volume fraction of stray grains was calculated for each of the cases and is reported in \Cref{tab:phi_ch7}. 
\begin{table}[h!]
\centering
 \begin{tabular}{||C{2.5cm} | C{6 cm} | C{3cm} ||} 
 \hline
 Preheat \linebreak Temperature & Physics & Volume fraction of equiaxed grains $\Phi$ \%\\ [0.5ex] 
 \hline\hline
 25 & HT & 15.46\\ 
 
 25 & HT + FF (Low Surfactant) & 24.3 \\ 
 
 25 & HT + FF (High Surfactant) & 29.7 \\
 
 1000 & HT & 74.84 \\ 
 
 1000 & HT + FF (Low Surfactant) & 78.01 \\
 
 1000 & HT + FF (High Surfactant) & 84.44  \\ [1ex] 
 \hline
\end{tabular}
\caption{Comparison of $\Phi$ value with and without fluid flow in the model}
\label{tab:phi_ch7}
\end{table}

It can be observed that presence of high surfactant concentration increases the probability of stray grain formation.
\subsection{Significance of Scan Strategy on 
Fluid Flow and Experimental Comparison of melt pool shape:}
To further understand the effect of fluid flow on the shape of the melt pool as a function of complex scan pattern, "5-spot" pattern described in \Cref{ch:chapter_4} (\Cref{5spot}) and by Raghavan et al.\cite{RAGHAVAN2017375} is used in the simulations. At each spot, the electron beam was turned ON for \SI{0.25}{\milli\second} with beam current of \SI{20}{\milli\ampere}. Two extreme cases with internal point offset (ref: \Cref{5spot}(b)) of \SI{200}{\micro\meter} and \SI{800}{\micro\meter} are used to analyze the effect of fluid flow on the size of the melt pool. In case 1 (internal point offset: \SI{200}{\micro\meter}), 5 melt pools combine to form single melt pool and in case 2, 5 independent melt pools are formed (\Cref{4_sample}). Two simulations of each case has been performed (pure heat transfer and fluid flow with high surfactant concentration). For, the fluid flow cases, the surface tension gradient described in \Cref{STGHS_eqn_ch7} is applied. Since the "5-spot" scan pattern exhibits four fold symmetry, one quadrant of the scan strategy is simulated and compared with the experimental data. 
 
\begin{figure}[!t]
\centering
\captionsetup{justification=centering}
\includegraphics[width=6in]{7_200_comparison.jpg}
\caption{Comparison of shape of simulated melt pool with internal point offset of \SI{200}{\micro\meter} (a) Pure heat transfer - top surface (XY plane) (b) Pure heat transfer - build direction (XZ plane) (c) heat transfer plus fluid flow (high surfactant) - top surface (XY plane) (d) heat transfer plus fluid flow (high surfactant) - build direction (XZ plane)}
\label{fig:7_200_comparison}
\end{figure}

\noindent\textbf{Case 1:} \Cref{fig:7_200_comparison} shows the comparison of the shape of the top surface of one quadrant simulated melt pool (Internal point offset: \SI{200}{\micro\meter}) of heat transfer only  (\Cref{fig:7_200_comparison} (a)) and with fluid flow (\Cref{fig:7_200_comparison} (b)). In this case, 5 spots within a domain of the scan strategy (\Cref{5spot}) merges to form single melt pool. \Cref{fig:7_200_comparison} (c) and (d) shows the shape of the melt pool along the build direction without and with fluid flow respectively. The simulation data is compared with the experimental data. \Cref{fig:7_200_keyence} (a) shows the surface topology of the melt pool depicting it`s width. \Cref{fig:7_200_keyence} (b) is the optical image of the etched sample showing the depth of the melt pool traces along the build direction. The measured depth of the melt pool will not be accurate as the 3-D data of the depth was not characterized. Serial sectioning of the sample has to be done to improve the accuracy of the data.   

\begin{figure}[!t]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_200_keyence.jpg}
\caption{Melt pool with internal point offset of \SI{200}{\micro\meter} (a) Top surface (XY plane) (b) Build direction (XZ plane)}
\label{fig:7_200_keyence}
\end{figure}

\noindent\textbf{Case 2:} Similarly, simulations has been done for the case 2 (Internal point offset: \SI{800}{\micro\meter}). In this case, 5 independent molten pools are formed within a domain of the scan strategy (\Cref{5spot}). \Cref{fig:7_800_comparison} shows the comparison of the shape of the top surface of one quadrant simulated melt pool (Internal point offset: \SI{800}{\micro\meter}) of heat transfer only  (\Cref{fig:7_800_comparison} (a)) and with fluid flow (\Cref{fig:7_800_comparison} (b)). \Cref{fig:7_800_comparison} (c) and (d) shows the shape of the melt pool along the build direction without and with fluid flow respectively. \Cref{fig:7_800_keyence} (a) shows the top surface of the melt pool depicting it`s width. \Cref{fig:7_800_keyence} (b) is the optical image of the sample showing the depth of the melt pool traces along the build direction.
  
\begin{figure}[!t]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_800_comparison.jpg}
\caption{Comparison of shape of simulated melt pool with internal point offset of \SI{800}{\micro\meter} (a) Pure heat transfer - top surface (XY plane) (b) Pure heat transfer - build direction (XZ plane) (c) heat transfer plus fluid flow (high surfactant) - top surface (XY plane) (d) heat transfer plus fluid flow (high surfactant) - build direction (XZ plane)}
\label{fig:7_800_comparison}
\end{figure} 


\begin{figure}[!t]
\centering
\captionsetup{justification=centering,margin=0.75cm}
\includegraphics[width=6in]{7_800_keyence.jpg}
\caption{Melt pool with internal point offset of \SI{800}{\micro\meter} (a) Top surface (XY plane) (b) Build direction (XZ plane)}
\label{fig:7_800_keyence}
\end{figure}

To analyze the data quantitatively, the width and depth of the melt pool were measured for different simulation cases and the experiment. The data was quantified using ImageJ software \cite{schneider2012nih}. Measured data are listed in \Cref{tab:shape_ch7}. It can be observed that for the case with \SI{800}{\micro\meter} internal point offset, there is no significant difference between the depth and width of the simulation data between the heat transfer only (HT) model and the model with the fluid flow (HT+FF). But, significant difference is observed between the 2 models for the case with \SI{200}{\micro\meter} internal point offset. This shows that the scan pattern has significant impact on the influence on the necessity of fluid flow in the model. Also, for both cases, the simulation tend to under predict the width of the melt pool. This can be attributed to the uncertainty in the surface tension data used in the simulations.
   
\begin{table}[h!]
\centering
 \begin{tabular}{||C{3cm} | C{3 cm} | C{3 cm} | C{3cm} ||} 
 \hline
 Internal Point Offset (\SI{}{\micro\meter})& Data Type & Depth (\SI{}{\micro\meter})& Width (\SI{}{\micro\meter})\\ [0.5ex] 
 \hline\hline
 800 & Experiment & 193$^*$ & 823 \\
 
 800 & HT & 178 (-7.7\%) & 606 (-26.4\%) \\ 
 
 800 & HT + FF & 184 (-4.7\%)& 620 (-24.6\%)\\ 
 
 \hline
 
 200 & Experiment & 390$^*$ & 1439 \\ 
 
 200 & HT & 300 (-23.1\%) & 910 (-36.7 \%)\\ 
 
 200 & HT + FF & 415 (+6.02\%) & 1124 (-21.8 \%)\\ [1ex] 
 \hline
\end{tabular}
\captionsetup{justification=centering,margin=0.75cm}
\caption{Experimental comparison of width and depth of the melt pool with and without fluid flow in the model}
\label{tab:shape_ch7}
\end{table}

\section{Summary}
Effect of fluid flow on a single spot melting has been studied in this chapter. In particular,

\begin{itemize}
  \item Effect of different concentration of surfactents on the fluid flow pattern and shape has been analyzed for extreme cases of preheat temperatures of 25\textdegree C and 1000\textdegree C. The results are compared with the simulations without fluid flow. 
  \item Competition between convective heat transport and conductive heat transport is analyzed by calculating spatially varying non-dimensional Peclet number.
  \item Volume fraction of stray grain was calculated and compared for low S, high S cases with pure heat transfer simulation. A maximum of 10\% difference was observed for the case with 1000\textdegree C while a maximum of 15\% was observed for the case with 25\textdegree C preheat.
  \item Effect of fluid flow on the shape and size of the melt pool was compared with the experimental data. In the "5-spot" melting strategy, for the scan strategy with smaller areal energy density, there is no significant difference in the shape and size of the melt pool between the heat transfer only model and the model with fluid flow turned ON. Significant difference was observed with higher areal density. It was concluded that in addition to the material dependent surface tension, the scan strategy also has significant impact on the necessity of the fluid flow in the model to improve the accuracy of the prediction of shape of the melt pool. Higher the energy density, more important the fluid flow is and vice versa.     
\end{itemize}